import UIKit

class LeftMenuTableCell: UITableViewCell {

    @IBOutlet weak var lblLeftIcon: UILabel!
    @IBOutlet weak var lblLeftTitle: LabelDesign!
    @IBOutlet weak var leadingSepratort: NSLayoutConstraint!
    @IBOutlet weak var lblLine: UILabel!
    @IBOutlet weak var imgIocn: UIImageView!

    @IBOutlet weak var lblSubItem: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
