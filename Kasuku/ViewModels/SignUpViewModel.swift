import Foundation
import SwiftyJSON

enum Country{
    case none
    case selected(String)
}

let kSignUpNumberOfSection : Int = 1
let kSignUpNumberOfRows : Int =  1
let kSignUpPickerComponent : Int = 1
let kSignUpHeight : CGFloat = 720
let kSignUpPickerHeight : CGFloat = 44

class SignUpViewModel: NSObject {
    
    var countries : [CountriesModel] = [CountriesModel]()
    var countrySelected : Country = .none
    var signUpDetails : SignUpModel!
    var termsAccepted: Bool = false

    override init() {
        super.init()
        signUpDetails = SignUpModel()
        self.getCountriesFromBundle()
    }
    

    var tableSection: Int{
        get{
            return kSignUpNumberOfSection
        }
    }
    
    var pickerComponent: Int{
        get{
            return kSignUpPickerComponent
        }
    }
    
    var pickerHeight: CGFloat{
        get{
            return kSignUpPickerHeight
        }
    }
    
    var tableRows: Int{
        get{
            return kSignUpNumberOfRows
        }
    }
    
    var tableHeight: CGFloat{
        get{
            return kSignUpHeight
        }
    }
    
    func getCountriesFromBundle(){
        if let filePath = Bundle.main.path(forResource: "countries", ofType: "json"), let data = NSData(contentsOfFile: filePath) {
            do {
                let json =  try JSONSerialization.jsonObject(with: data as Data, options: .allowFragments)
                let countriesJSON = JSON(json)
                let countriesAvailable = countriesJSON.arrayValue
                if countriesAvailable.count > 0{
                    self.countries.removeAll()
                    for cCount in countriesAvailable{
                        self.countries.append(CountriesModel(json: cCount))
                    }
                }
            }catch let error {
                print(error)
            }
        }
    }
    
    func titleForPicker(at row : Int)->String{
        guard let name = countries[row].name else {return ""}
        countrySelected = Country.selected(name)
        return name
    }
    
    func codeForPicker(at row : Int)->String{
        guard let name = countries[row].code else {return ""}
        return name
    }
    
    func didSelect(at row : Int){
        guard let name = countries[row].name else {return}
        countrySelected = Country.selected(name)
    }
    
    func validate()-> (Bool, String){
        
        if let firstName = self.signUpDetails.firstName, firstName.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return (false, "Enter First Name")
        }
        
        if let lastName = self.signUpDetails.lastName, lastName.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return (false, "Enter Last Name")
        }
        
        if case Country.none = countrySelected{
            return (false, "Select Country")
        }
        
        if let mobileNumber = self.signUpDetails.mobileNumber,  mobileNumber.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return (false, "Enter Mobile Number")
        }
        
//        if let mobileNumber = self.signUpDetails.mobileNumber,  mobileNumber.trimmingCharacters(in: .whitespacesAndNewlines).count < 10{
//            return (false, "Enter Correct Mobile Number")
//        }
        
        if let emailID = self.signUpDetails.emailID,  emailID.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return (false, "Enter Email ID")
        }
        
        if let emailID = self.signUpDetails.emailID,  !emailID.isValidEmail(){
            return (false, "Enter Correct Email ID")
        }
        
        if let password = self.signUpDetails.password,  password.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return (false, "Enter Password")
        }
        
        if let confirmPassword = self.signUpDetails.confirmPassword,  confirmPassword.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return (false, "Enter Confirm Password")
        }
        
        if let password = self.signUpDetails.password,  let confirmPassword = self.signUpDetails.confirmPassword, password != confirmPassword {
            return (false, "Entered Password and Confirm Password Doesn't match")
        }
        
        if !termsAccepted{
            return (false, "Please accept terms and conditions")
        }
        
        if case Country.selected(let value) = countrySelected{
            return (true, value)
        }
        return (false, "")
    }
    
    
    func createUser(country: String, accountCreationSource : String, token : String,completionHandler:@escaping (Bool,String?)->Void){
        let url = Constant.serverURL + Constant.EndPoints.signUp
        let name = self.signUpDetails.firstName! + " " + self.signUpDetails.lastName!
        let params = ["device_id": Constant.deviceID, "name" : name, "password": self.signUpDetails.password!, "email": self.signUpDetails.emailID!, "contactNumber": self.signUpDetails.mobileNumber!, "country":country, "accountCreationSource":accountCreationSource, "token": token]
        NetworkManager.sharedInstance.commonNetworkCall(url: url, method: .post, parameters: params) { (json, error) in
            guard let vJSON = json else {return completionHandler(false, error!)}
                if vJSON["success"].boolValue{
                    let dict = vJSON["data"]
                    print(dict)
                    //Apoorva
                    UserDefaults.standard.set(dict["country"], forKey: Constant.DefaultStorage.USER_COUNTRYNAME)
                    UserDefaults.standard.set(dict["name"], forKey: Constant.DefaultStorage.USER_NAME)
                    UserDefaults.standard.set(dict["contactNumber"], forKey: Constant.DefaultStorage.USER_CONTACTNUMBER)
                    UserDefaults.standard.set(dict["email"], forKey: Constant.DefaultStorage.USER_EMAIL)
                    UserDefaults.standard.synchronize()
                   
                    completionHandler(true, nil)
                }else{
                    completionHandler(false, vJSON["message"].stringValue)
                }
            }
    }
    
}

