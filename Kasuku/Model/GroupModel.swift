import Foundation

class GroupModel: NSObject {
    var id: String? = ""
    var color:String? = ""
    var desc: String? = ""
    var group_id: NSNumber = 0
    var group_shared_channel: String? = ""
    var members = [String]()
    var name: String? = ""
    var owner: String? = ""
    var pending_members = [String]()
    var type: String? = "group"
    
    override init() {
        super.init()
    }
    
    init(id: String, color:String, desc: String, group_id: NSNumber,group_shared_channel: String,members: [String],name: String,owner: String,pending_members: [String]) {
        self.id = id
        self.color = color
        self.desc = desc
        self.group_id=group_id
        self.group_shared_channel = group_shared_channel
        self.members = members
        self.name = name
        self.owner = owner
        self.pending_members = pending_members
        
    }
    
}
