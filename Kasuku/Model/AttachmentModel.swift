import Foundation

class AttachmentModel: NSObject {
    
    var attachment_id: NSNumber = 0
    var uri: String? = ""
    var name: String? = ""
    var bucket_key: String? = ""
    var size: NSNumber = 0
    var length: NSNumber = 0
    var mime_type: String? = ""
    var sync: Bool = false
    var note_id: String? = ""
    var owner: String? = ""
    var type: String? = "attachment"
    var attachementData: Data? 
    var formate: String?

    
    override init() {
        super.init()
    }
    
    init(attachment_id: NSNumber, uri: String, name: String, bucket_key: String, size: NSNumber, length: NSNumber, mime_type: String, sync: Bool, note_id: String, owner: String){
        
        self.attachment_id = attachment_id
        self.uri = uri
        self.name = name
        self.bucket_key = bucket_key
        self.size = size
        self.length = length
        self.mime_type = mime_type
        self.sync = sync
        self.note_id = note_id
        self.owner = owner
//        self.attachementData = dataObj
        
    }
    
}
