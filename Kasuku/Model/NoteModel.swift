import Foundation

class NoteModel: NSObject {
    var id: String? = ""
    var address: String? = ""
    var alarm: String? = ""
    var archived: Bool = false
    var attachments = [AttachmentModel]()
    var checklist: Bool = false
    var content: String? = ""
    var creation: NSNumber = 0
    var last_modification: NSNumber = 0
    var group_id: String? = nil
    var latitude:NSNumber = 0
    var locked:Bool = false
    var longitude:NSNumber = 0
    var owner:String = ""
    var recurrence_rule:String? = nil
    var reminder_fired: Bool = false
    var title: String? = ""
    var trashed: Bool = false
    var category_id: String? = ""
    var type: String? = "note"
    var dateCreate:String = ""

    
    override init() {
        super.init()
    }
    
    init(
        id: String, address: String, alarm: String, archived: Bool = false,
        checklist: Bool, content: String, creation: NSNumber, last_modification: NSNumber, group_id: String, latitude:NSNumber, locked:Bool, longitude:NSNumber, owner:String, recurrence_rule:String, reminder_fired: Bool, title: String, trashed: Bool, category_id: String
        ) {
        self.id = id
        self.address = address
        self.alarm = alarm
        self.archived = archived
//        self.attachments = attachments
        self.checklist = checklist
        self.content = content
        self.creation = creation
        self.last_modification = last_modification
        self.group_id = group_id
        self.latitude = latitude
        self.locked = locked
        self.longitude = longitude
        self.owner = owner
        self.recurrence_rule = recurrence_rule
        self.reminder_fired = reminder_fired
        self.title = title
        self.trashed = trashed
        self.category_id = category_id
        
        
    }
    
    
    
}
