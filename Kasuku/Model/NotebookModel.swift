import Foundation
class NotebookModel: NSObject {

   var id: String? = ""
    var category_id : NSNumber = 0
    var color : String? = ""
    var desc : String? = ""
    var name : String? = ""
    var owner: String? = ""
    var type : String? = "category"
    
    override init() {
        super.init()
    }
    
     init(id:String, category_id : NSNumber, color : String,  desc : String,name : String, owner: String) {
        self.id = id
        self.category_id = category_id
        self.color = color
        self.desc = desc
        self.name = name
        self.owner = owner
        
    }
    
}
