import Foundation
import Alamofire
import SwiftyJSON
import UIKit
import SystemConfiguration

/**
 *  NSObject Custom Class (NetworkManager) for Network Related Task.
 *
 *  @superclass NSObject
 */
public class NetworkManager:NSObject {
    
    //MARK:- Properties
    static let sharedInstance = NetworkManager()

    //MARK:- Method
    /**
     *  Method for Check Internet Connectivity
     *
     *  @return Bool
     */
    func isInternetAvailable() -> Bool{
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    /**
     *  Method for Common Network Service Call
     *
     *  @param url String
     *  @param method HTTPMethod
     *  @param parameters [String:AnyObject]?
     *  @escaping completionHandler (JSON?,String?)
     *  @return void
     *
     */
    func commonNetworkCall(url:String,method:HTTPMethod,parameters : [String:Any]?,completionHandler:@escaping (JSON?,String?)->Void) {
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        Alamofire.request(url, method:method,parameters:parameters,encoding:URLEncoding.default).responseJSON { (response) in
            if(response.result.isSuccess){
                if let data = response.result.value{
                    let json = JSON(data)
                    completionHandler(json,nil)
                    return
                }
            }
            completionHandler(nil,response.result.error?.localizedDescription)
        }
    }
    
    func commonNetworkCallInForm(url:String,method:HTTPMethod,parameters : [String:Any]?, images : [UIImage],completionHandler:@escaping (JSON?,String?)->Void){
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        Alamofire.upload(multipartFormData: { (formData) in
            
            for (index,subImage) in images.enumerated(){
                let imageData = UIImageJPEGRepresentation(subImage, 0.3)!
                formData.append(imageData, withName: "listImage\(index)", fileName: "fileName\(index).jpg", mimeType: "image/jpg")
            }
            
            
            
            let imagesCount = images.count
            let iData = String(describing: imagesCount)
            let iSData = iData.data(using: .utf8)
            formData.append(iSData!, withName: "countImage")
            
            for (key,value) in parameters!{
                let components = String(describing: value)
                let data = (components).data(using:.utf8)
                formData.append(data!, withName: key)
            }
        },to: url ,method:method,
          encodingCompletion:{ (encodingResult) in
            switch encodingResult {
            case .success(let upload,_ , _):
                upload.responseJSON { response in
                    if let data = response.result.value {
                        let json = JSON(data)
                        completionHandler(json,nil)
                    }else{
                        completionHandler(nil,"Internet Connection Error.")
                    }
                }
            case .failure(let encodingError):
                completionHandler(nil,encodingError.localizedDescription)
            }
            
        })
    }
    
    func commonNetworkCallInFormVideo(url:String,method:HTTPMethod,parameters : [String:Any]?, images : [UIImage], video : Data,completionHandler:@escaping (JSON?,String?)->Void){
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        Alamofire.upload(multipartFormData: { (formData) in
            
            for (index,subImage) in images.enumerated(){
                let imageData = UIImageJPEGRepresentation(subImage, 0.3)!
                formData.append(imageData, withName: "listImage\(index)", fileName: "fileName\(index).jpg", mimeType: "video/jpg")
            }
            
            
            formData.append(video, withName: "uploadVideo", fileName: "Video", mimeType: "video/mov")
            
            let imagesCount = images.count
            let iData = String(describing: imagesCount)
            let iSData = iData.data(using: .utf8)
            formData.append(iSData!, withName: "countImage")
            
            for (key,value) in parameters!{
                let components = String(describing: value)
                let data = (components).data(using:.utf8)
                formData.append(data!, withName: key)
            }
        },to: url ,method:method,
          encodingCompletion:{ (encodingResult) in
            switch encodingResult {
            case .success(let upload,_ , _):
                upload.responseJSON { response in
                    if let data = response.result.value {
                        let json = JSON(data)
                        completionHandler(json,nil)
                    }else{
                        completionHandler(nil,"Internet Connection Error.")
                    }
                }
            case .failure(let encodingError):
                completionHandler(nil,encodingError.localizedDescription)
            }
            
        })
    }
    
    
    func commonNetworkCallWithHeader(header :[String:String],url:String,method:HTTPMethod,parameters : [String:Any]?,completionHandler:@escaping (JSON?,String?)->Void) {
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if(response.result.isSuccess){
                if let data = response.result.value{
                    let json = JSON(data)
                    completionHandler(json,nil)
                    return
                }
            }
            completionHandler(nil,response.result.error?.localizedDescription)
        }
    }
    
    
    func commonNetworkCallToUploadImage(imageData : Data,header :[String:String]?,url:String,method:HTTPMethod,parameters : [String:Any]?,completionHandler:@escaping (JSON?,String?)->Void) {
        let configuration = URLSessionConfiguration.background(withIdentifier: "com.elika.version2.background")
        configuration.timeoutIntervalForRequest = 120
        configuration.timeoutIntervalForResource = 120
        let manager = Alamofire.SessionManager(configuration: configuration)
        manager.startRequestsImmediately = true
        Alamofire.upload(multipartFormData: { (formData) in
            let timeStamp = Date().timeIntervalSinceReferenceDate
            formData.append(imageData, withName: "profilePic", fileName: "unknownImage\(timeStamp)", mimeType: "image/jpg")
            for (key,value) in parameters!{
                let components = String(describing: value)
                let data = (components).data(using:.utf8)
                formData.append(data!, withName: key)
            }
        },to: url ,method:method,
          headers:header,
          encodingCompletion:{ (encodingResult) in
            switch encodingResult {
            case .success(let upload,_ , _):
                upload.responseJSON { response in
                    if let data = response.result.value {
                        let json = JSON(data)
                        completionHandler(json,nil)
                    }else{
                        completionHandler(nil,"Internet Connection Error.")
                    }
                }
            case .failure(let encodingError):
                completionHandler(nil,encodingError.localizedDescription)
            }
            
        })
        
    }
    
} //Class ends here





