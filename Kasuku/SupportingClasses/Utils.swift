import Foundation

//MARK:- TextFieldType
enum TextFieldType: Int {
    case Username = 0
    case Password
    case Email
}
