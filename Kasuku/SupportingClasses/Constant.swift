import UIKit

public struct Constant{

    //static let serverURL = "http://18.219.75.39:8080/"
    //static let ImagePath = "http://18.219.75.39:8080/api/get/attachment?id="
    
    static let serverURL = "http://18.222.43.228:8080/"
    static let ImagePath = "http://18.222.43.228:8080/api/get/attachment?id="

    
    static let deviceID : String = UIDevice.current.identifierForVendor!.uuidString
    
    //MARK: -  User Defautls
    struct UserDefaultKeys{
        static let userToken  =  "kUserToken"
        static let loggedIn  =   "kLoggedIn"
    }
    
    // MARK:- Variable
    struct Variable {
        static let camera                    = "Camera"
        static let photoLibrary              = "Photo Galary"
        static let cancel                    = "Cancel"
    }
    
    //MARK:- Alert Messages
    struct AlertMessages{
        static let somethingWent           = "Something went wrong, Please check your internet connection."
        static let callDisconnected        = "This call is already been disconnected by the other user."
        static let selectDate              = "Seems like you haven't selected any dates, Please select atleast one date for key sharing."
        static let noEmailAssociated       = "There is no Email associated with this Contact, Please go to contacts and add email for key sharing."
        static let noBleCode               = "Seems like you don't have proper user credential for unlocking this door,Contact administrator for further enquiry."
        static let selectLock              = "Seems like you haven't selected any gate for sharing your key."
        static let noShare                 = "Seems like you don't have any lock for sharing. Please configure at least one lock and try again."
        static let turnOn                  = "Seems like your Bluetooth services are disabled,please go to Settings and turn on Bluetooth Services."
        static let deviceProximityOut      = "seems to be out of your selected sensitivity."
        static let autoGateOpen            = "Congratulations, Lock opened."
        static let manualGateOpen          = " lock opened."
        static let manualGateFound         = "Lock Found, Please unlock it manually."
        static let manualDes               = "In Notification Mode you will have to manually move the slider to unlock the Elika Device."
        static let autoDes                 = "In Auto Mode the Lock will open automatically when you are  in close proximity to the Elika Device."
        static let manualMode              = "Notification Mode."
        static let autoMode                = "Auto Mode."
        static let deleteKey               = "Delete Key."
        static let imagePickMessage        = "Please select the way to upload the picture."
        static let setChoice               = "Select Choice."
        static let failConnect             = "Fail to connect to BLE, make sure your ble is in your range and try again later."
        static let bluetoothOff            =  "Seems like your bluetooth is disabled,please go to Settings and turn on Bluetooth."
        static let passResetSuccess        = "Your passoword reset Successfully."
        static let passChangeSuccess       = "Your passoword changed Successfully."
        static let passwordLength          = "Please enter password which at least 7 or more character long."
        static let selectOneTime           = "You need to add at least one set of timings to share your key."
        static let successBLE              = "You have successfully configured your BLE key"
        static let password                = "Password"
        static let wifiPass                = "Enter your wifi password for "
        static let shareKey                = " to share your BLE key"
        static let groupAdded              = "These contacts successfully added to your new group."
        static let slowDown                = "Please wait, until year is selected."
        static let chooseName              = "Looks like you haven't added name to your group. Please add and proceed."
        static let chooseOne               = "Looks like you haven't selected any contact or group. Please select at least one to proceed."
        static let chooseContact           = "Looks like you haven't selected any contact. Please select at least one to proceed."
        static let passwordMismatch        = "Password and Confirm password must match."
        static let passwordMismatchOld        = "Seems like you have entered the wrong old password, make sure the password is correct and try again."
        static let mendatoryField          = "Please fill all the mendatory fields."
        static let otpLength               = "Please enter your correct 5-digit one time password"
        static let contactAuthorization    = "This app previously was refused permissions to contacts; Please go to settings and grant permission to this app so it can use contacts."
        static let photoAuthorization      = "This app previously was refused permissions to photos; Please go to settings and grant permission to this app so it can use photos."
        static let invalidEmail            = "Please enter correct email ID."
        static let thankyou                = "ThankYou!"
        static let congo                   = "Congratulations!"
        static let successfullyUpdated     = "Your Profile has been updated successfully."
        static let successfullyUpdatedImage     = "Group image has been updated successfully."
        static let alertTitle              = "Oops!"
        static let key_uath                = "Key is unauthorized to open the Lock."
        static let wait                    = "Wait!"
        static let wifi                    = "Let us take you to settings so you could select the wifi network to configure lock or hit OK to go with default settings."
        static let wifiOff                 = "Seems like your wifi is disabled,please go to Settings and turn on WIFI."
        static let successInvitation       = "We'll send an invitation to your friends once you post this Plan!"
        static let planPostText            = "You can see all your plans in your Profile"
        static let changePlansText         = "Change of Plans?"
        static let updatedPlansText        = "If you continue, your current plans will be updated."
        static let sureText                = "Are you sure?"
        static let proUnmatch              = "You want to unmatch this profile."
        static let successUnmatch          = "You have successfully unmatched this user"
        static let report                  = "You want to report this user."
        static let sessionExpiredText      = "Seems like your session has been expired, Please login again to continue."
        static let loctnSendText           = "Are you sure you want to send your location?"
        static let whatYouDoText           = "What would you like to do?"
        static let pleaseSelText           = "Please select :"
        static let removePlanText          = "You want to remove your plan"
        static let unjoinPlanText          = "You want to unjoin this plan"
        static let unjoinPlanMesg          = "Press below button to unojin this plan"
        static let enableInternet            = "Seems like your internet services are disabled,please go to Settings and turn on Internet Services."
        static let locationSettingsMesg    = "Seems like your app is not having authority to fetch current location, please change your settings."
        static let editGroupNameMsg        = "Please enter group name here."
        static let edit                    = "Rename Group."
        static let delete                  = "Delete Group."
        static let deleteMember            = "Delete Member."
        static let deleteMemberMsg         = "Are you sure you want to delete this member from this group?"
        static let deleteSuccess           = "Group Deleted."
        static let sessionExpired          = "Session Expired."
        static let sessionExpiredMsg       = "Your session has been expired, Please login again to continue."
        static let deleteMsg               = "Are you sure you want to delete this group?"
        static let noLock                  = "No Lock available for now, Please configure one and try again."
    }
    
    struct Plist {
      static let podsAcknowledgements      = "Pods-Urban-acknowledgements"
    }
    
    struct Notifications {
        static let inviteFriendsAlertNotf  = "InviteFriendsShowAlertNotification"
    }
    
    struct StoryBoard{
        static let main                     = UIStoryboard(name: "Main", bundle: nil)
        static let auth                     = UIStoryboard(name: "Authentication", bundle: nil)
        static let onboard                  = UIStoryboard(name: "Onboarding", bundle: nil)
    }
    
    struct ScreenDimension {
        static let width                    = UIScreen.main.bounds.size.width
        static let height                   = UIScreen.main.bounds.size.height
    }
    
    struct Strings{
        static let noDevAvailable           = "Seems like you don't have any locks to configure."
        static let pleaseScan               = "Please search to look up for devices."
        static let availDevices             = "Available devices."
        static let whiteSpace               = " "
        static let blank                    = ""
        static let newline                  = "\n"
        static let fbPermText               = "We never post on Facebook without your permission."
        static let swipedEveryone           = "You’ve swiped everyone going so far."
        static let plansNCreated            = "Looks like you haven’t made any plans."
        static let noAttendeesText          = "There are no attendees to this plan."
        static let workHeaderText           = "If your current job isn't shown, please update it on Facebook and it will appear here."
        static let educationHeaderText      = "If your current education isn't shown, please update it on Facebook and it will appear here."
        static let messageSubject         = "Hey message subject goes here"
        static let emailSubject        = "Hey email subject goes here"
        static let initialText         = "Hey initial text goes here"
        static let urlSchemes          = ["http", "https"]
    }
    
    struct DateFormat{
        static let seconds   = "yyyy-MM-dd'T'HH:mm:ssZ"
        static let miniSecs  = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        static let withYr    = "YYYY-MM-dd"
        static let planDetail = "EEE, MMM dd"
    }
    
    struct Dimensions{
        
        static let characterLimit               = 160
        static let mediumCharLimit              = 200
        static let largeCharLimit               = 400
        static let navHeight       :CGFloat     = 64
        static let textviewMargin  :CGFloat     = 18
    }
    
    struct Images{
        static let pro_checked           = "profile_checked_icon"
        static let userPlaceholder       = "user_placeholder"
    }
    
    public struct Identifiers {
        static let emptyDSetsCell      = "EmptySetsTableCell"
        static let centerNav           = "CenterNavigationController"
        static let loginNav            = "LoginNavigationController"
    }
    
    struct ElikaFont{
        static let regular              = "Lato-Regular"
        static let light                =  "Lato-Light"
        
    }
    
    struct EndPoints{
        static let login = "api/user/login"
        static let signUp   = "api/user/create"
        static let forgotPassword = "api/user/forgotpassword"
        static let attachment = "api/upload/attachment"
        static let updateProfile = "api/profile/save"

    }
    
    // DATABASE CONSTANTS
    struct DocumentTypes {
        static let TYPE_NOTE = "note"
        static let TYPE_CATEGORY = "category"
        static let TYPE_GROUP = "group"
        static let TYPE_ATTACHMENT = "attachment"
    }
    
    struct DatabaseKeys {
        static let KEY_DELETED = "_deleted"
        static let KEY_REVISION = "_rev"
        static let KEY_REMOVED = "_removed"
    }
    
    struct DocumentNoteKeys {
         static let KEY_ID = "_id"
         static let KEY_CREATION = "creation"
         static let KEY_LAST_MODIFICATION = "last_modification"
         static let KEY_TITLE = "title"
         static let KEY_CONTENT = "content"
         static let KEY_ARCHIVED = "archived"
         static let KEY_TRASHED = "trashed"
         static let KEY_REMINDER = "alarm"
         static let KEY_REMINDER_FIRED = "reminder_fired"
         static let KEY_RECURRENCE_RULE = "recurrence_rule"
         static let KEY_LATITUDE = "latitude"
         static let KEY_LONGITUDE = "longitude"
         static let KEY_ADDRESS = "address"
         static let KEY_CATEGORY = "category_id"
         static let KEY_LOCKED = "locked"
         static let KEY_CHECKLIST = "checklist"
         static let KEY_ATTACHMENTS = "attachments"
         static let KEY_DOC_TYPE = "type"
        
         static let KEY_DOC_OWNER = "owner"
         static let KEY_GROUP_ID = "group_id"
    }
    
    struct DocumentAttachmentKeys {
        static let KEY_ATTACHMENT_ID = "attachment_id"
        static let KEY_ATTACHMENT_URI = "uri"
        static let KEY_ATTACHMENT_NAME = "name"
        static let KEY_ATTACHMENT_BUCKET_KEY = "bucket_key"
        static let KEY_ATTACHMENT_SIZE = "size"
        static let KEY_ATTACHMENT_LENGTH = "length"
        static let KEY_ATTACHMENT_MIME_TYPE = "mime_type"
        static let KEY_ATTACHMENT_SYNC = "sync"
        static let KEY_ATTACHMENT_NOTE_ID = "note_id"
    }
    
    struct DocumentGroupKeys {
        static let KEY_GROUP_ID = "group_id"
        static let KEY_GROUP_NAME = "name"
        static let KEY_GROUP_SHARED_CHANNEL = "group_shared_channel"
        static let KEY_GROUP_DESCRIPTION = "description"
        static let KEY_GROUP_COLOR = "color"
        static let KEY_GROUP_MEMBERS = "members"
        static let KEY_GROUP_PENDING_MEMBERS = "pending_members"
    }
    
    // tags
    struct DocumentCategoryKeys {
        static let KEY_CATEGORY_ID = "category_id"
        static let KEY_CATEGORY_NAME = "name"
        static let KEY_CATEGORY_DESCRIPTION = "description"
        static let KEY_CATEGORY_COLOR = "color"
    }
    
    struct DefaultStorage {
        static let DB_NAME = "kasuku-notes"
        static let IS_LOGGED_IN = "isLoggedIn"
        static let USER_NAME = "username"
        static let USER_EMAIL = "useremail"
        static let USER_COUNTRYNAME = "countryName"
        static let USER_CONTACTNUMBER = "contactNumber"
        static let USER_CREATION = "createdAt"
        static let USER_ID = "userId"
        static let USER_PROFILE = "userProfile"
       //Apoorva
        static var isFromLeftMenu = ""

    }
    
    
    
    
    struct Header{
        static let key    = "Authorization"
    }
    
    // MARK:- Range Sensitivity
    struct RangeSensitivity{
        static let rangeParam                       =   ["Immediate", "Near","Far"]
        static let rangeVal                         =   [45,70,85]
        static let rangeDecs                        =   ["upto -45","upto -70", "upto -90"]
    }
}
