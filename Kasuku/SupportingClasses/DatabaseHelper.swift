import Foundation
import Alamofire

import CouchbaseLiteSwift

class DbHelper {
    
    private var database: Database? = nil
    private var replicator: Replicator?
    private var username: String? = nil
    private var pass: String?   = nil
    
    
    public func initDb(uname:String, upass: String){
        do {
            database = try Database(name: Constant.DefaultStorage.DB_NAME)
            self.username = uname
            self.pass = "KASUKU_\(upass)"
            self.sync()
            
        } catch {
            fatalError("Error opening database")
        }
    }
    
    
    public func sync(){
        if((self.username != nil) && (self.pass != nil)){
            let url = URL(string: "ws://18.219.75.39:4984/db")!
            let target = URLEndpoint(url: url)
            let config = ReplicatorConfiguration(database: database!, target: target)
            config.replicatorType = .pushAndPull
            
            config.authenticator = BasicAuthenticator(username: self.username!, password: self.pass!)
            self.replicator = Replicator(config: config)
            
            
            //             replication change handler
            self.replicator!.addChangeListener { (change) in
                
                if change.status.activity == .busy {
                    print("Replication is working")
                }
                
                if change.status.activity == .stopped {
                    print("Replication Completed")
                }
            }
             
            self.replicator!.start()

        }
    }
    
    
    public func saveNoteWithAttachment(note: NoteModel, attachs: [AttachmentModel]){
        let note_id = self.saveNote(note: note)
        do{
        if(note_id.count > 0){
            
            guard let document = database?.document(withID: note_id) else { return }
            let mutableDocument = document.toMutable()
            
            for attach in attachs{
                let attach_id = self.saveAttachment(attachment: attach, note_id: note_id)
                mutableDocument.array(forKey: Constant.DocumentNoteKeys.KEY_ATTACHMENTS)?.addString(attach_id)
            }
            try database?.saveDocument(mutableDocument)
        }
        }catch let err{
            fatalError("Error saving note with attachment \(err.localizedDescription)")
        }
        
    }
    
    //CREATE
    public func saveNote(note: NoteModel) -> String{
        var note_id:String = ""
        let mutableNoteDoc = MutableDocument()
            .setString(note.address, forKey: Constant.DocumentNoteKeys.KEY_ADDRESS)
            .setString(note.alarm, forKey: Constant.DocumentNoteKeys.KEY_REMINDER)
            .setBoolean(note.archived, forKey: Constant.DocumentNoteKeys.KEY_ARCHIVED)
            .setArray(MutableArrayObject(), forKey: Constant.DocumentNoteKeys.KEY_ATTACHMENTS)
            .setBoolean(note.checklist, forKey: Constant.DocumentNoteKeys.KEY_CHECKLIST)
            .setString(note.content, forKey: Constant.DocumentNoteKeys.KEY_CONTENT)
            .setNumber((Date().timeIntervalSince1970*1000 as NSNumber), forKey: Constant.DocumentNoteKeys.KEY_CREATION)
            .setString(note.group_id, forKey: Constant.DocumentNoteKeys.KEY_GROUP_ID)
            .setNumber(note.latitude, forKey: Constant.DocumentNoteKeys.KEY_LATITUDE)
            .setNumber(note.longitude, forKey: Constant.DocumentNoteKeys.KEY_LONGITUDE)
            .setBoolean(note.locked, forKey: Constant.DocumentNoteKeys.KEY_LOCKED)
            .setString(note.owner, forKey: Constant.DocumentNoteKeys.KEY_DOC_OWNER)
            .setString(note.recurrence_rule, forKey: Constant.DocumentNoteKeys.KEY_RECURRENCE_RULE)
            .setBoolean(note.reminder_fired, forKey: Constant.DocumentNoteKeys.KEY_REMINDER_FIRED)
            .setString(note.title, forKey: Constant.DocumentNoteKeys.KEY_TITLE)
            .setBoolean(note.trashed, forKey: Constant.DocumentNoteKeys.KEY_TRASHED)
            .setString(note.type, forKey: Constant.DocumentNoteKeys.KEY_DOC_TYPE)
            .setString((note.category_id ?? nil), forKey: Constant.DocumentNoteKeys.KEY_CATEGORY)
            .setArray(MutableArrayObject(), forKey: "channels")
        
        
        let channel: String = "channel:"+mutableNoteDoc.string(forKey: Constant.DocumentNoteKeys.KEY_DOC_OWNER)!
        mutableNoteDoc.array(forKey: "channels")!.addString(channel)
        
        do {
            try database?.saveDocument(mutableNoteDoc)
            note_id = mutableNoteDoc.id
        } catch {
            fatalError("Error saving document")
        }
        return note_id
    }
    
    public func saveAttachment(attachment: AttachmentModel, note_id: String) -> String{
        let attach_id:String = ""
        _ = attachment.uri!.split{$0 == "/"}.map(String.init).last
        
        if let url = URL(string: attachment.uri!){
                    do {
                        
                        
                        attachment.note_id = note_id
                        
                        let mutableAttachmentDoc = MutableDocument()
                            .setNumber(attachment.attachment_id, forKey: Constant.DocumentAttachmentKeys.KEY_ATTACHMENT_ID)
                            .setString(attachment.note_id, forKey: Constant.DocumentAttachmentKeys.KEY_ATTACHMENT_NOTE_ID)
                            .setString(attachment.owner, forKey: Constant.DocumentNoteKeys.KEY_DOC_OWNER)
                            .setString(attachment.name, forKey: Constant.DocumentAttachmentKeys.KEY_ATTACHMENT_NAME)
                            .setBoolean(attachment.sync, forKey: Constant.DocumentAttachmentKeys.KEY_ATTACHMENT_SYNC)
                            .setString(attachment.type, forKey: Constant.DocumentNoteKeys.KEY_DOC_TYPE)
                            .setString(attachment.uri, forKey: Constant.DocumentAttachmentKeys.KEY_ATTACHMENT_URI)
                            .setString(attachment.mime_type, forKey: Constant.DocumentAttachmentKeys.KEY_ATTACHMENT_MIME_TYPE)
                            .setArray(MutableArrayObject(), forKey: "channels")
                        
                        let channel: String = "channel:"+mutableAttachmentDoc.string(forKey: Constant.DocumentNoteKeys.KEY_DOC_OWNER)!
                        mutableAttachmentDoc.array(forKey: "channels")!.addString(channel)
                        
                        do {
                            try database?.saveDocument(mutableAttachmentDoc)
                            print("Document saved successfully with id: \(mutableAttachmentDoc.id)")
                            
                            self.uploadeFileToServer(dirPath: attachment.uri!, userId: "\(attachment.owner ?? "")", attachmentId: "\(mutableAttachmentDoc.id)", objectId: "\(attachment.note_id ?? "")", fileData: attachment.attachementData, mimeType: attachment.mime_type, formate: attachment.formate) { (json) in
                                
                            
                            
                                let bukkedid = json!["data"] as? String
                                self.setAttachementData(bukkedid: bukkedid!, mutableAttachmentDoc: mutableAttachmentDoc)
//                                mutableAttachmentDoc.setString(bukkedid, forKey: Constant.DocumentAttachmentKeys.KEY_ATTACHMENT_BUCKET_KEY)
//
//                                try database?.saveDocument(mutableAttachmentDoc)
//
//                                attach_id = mutableAttachmentDoc.id

                            }
                            

//                            let bucket = "\(attachment.owner ?? "")/\(attachment.note_id ?? "")/\(mutableAttachmentDoc.id)\(file_name ?? "")"
//                            print("Bucket created is: \(bucket)")
//                            //let bucket = attachment.owner+"/"+attachment.note_id+"/"+mutableAttachmentDoc.id+""+file_name+""
//                            mutableAttachmentDoc.setString(bucket, forKey: Constant.DocumentAttachmentKeys.KEY_ATTACHMENT_BUCKET_KEY)
//
//                            try database?.saveDocument(mutableAttachmentDoc)
//
//                            attach_id = mutableAttachmentDoc.id
                        } catch {
                            fatalError("Error saving document")
                        }
                        
                    }catch let err{
                        print("Error: \(err.localizedDescription)")
                    }
                }
        
        return attach_id
        
        
    }
    func setAttachementData(bukkedid: String, mutableAttachmentDoc: MutableDocument )  {
        print(bukkedid)
        mutableAttachmentDoc.setString(bukkedid, forKey: Constant.DocumentAttachmentKeys.KEY_ATTACHMENT_BUCKET_KEY)
        do {
            try database?.saveDocument(mutableAttachmentDoc)
        } catch {
            fatalError("Error saving document")
        }
        
        //                                        attach_id = mutableAttachmentDoc.id
        
    }
    
    func getImage(path: String) -> UIImage?{
        let fileManager = FileManager.default
        let imagePAth = path
        if fileManager.fileExists(atPath: imagePAth){
            return UIImage(contentsOfFile: imagePAth)
        }else{
            print("No Image")
            return nil
        }
    }
    func uploadeFileToServer(dirPath: String, userId: String, attachmentId: String, objectId: String, fileData: Data?, mimeType:String?, formate: String?, completed: @escaping([String: Any]?) -> ())  {
//        let imageURL = URL(fileURLWithPath: dirPath)
//        let image    = UIImage(contentsOfFile: imageURL.path)
//        let imageData = UIImageJPEGRepresentation(image!, 0.5)

        let url : String = Constant.serverURL + Constant.EndPoints.attachment
        let parameter = [
            "user": userId,
            "objectId": objectId,
            "attachmentId" : attachmentId
        ]
       // multipartFormData.append(imageData!, withName: "file")
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(fileData!, withName: "file", fileName: formate!, mimeType: mimeType!)

            for (key, value) in parameter {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: url, encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    if let jsonResponse = response.result.value as? [String: Any] {
                        print(jsonResponse)
                        completed(jsonResponse)

                    }
                }
            case .failure(let encodingError):
                print(encodingError)
                completed(nil)

                
            }
        }
        )
        
        
    }
    
    public func saveGroup(group: GroupModel){
        let mutableGroupDoc = MutableDocument()
            .setString(group.color, forKey: Constant.DocumentGroupKeys.KEY_GROUP_COLOR)
            .setString(group.desc, forKey: Constant.DocumentGroupKeys.KEY_GROUP_DESCRIPTION)
            .setNumber(group.group_id, forKey: Constant.DocumentGroupKeys.KEY_GROUP_ID)
            .setString(group.group_shared_channel, forKey: Constant.DocumentGroupKeys.KEY_GROUP_SHARED_CHANNEL)
            .setArray(MutableArrayObject(), forKey: Constant.DocumentGroupKeys.KEY_GROUP_MEMBERS)
            .setString(group.name, forKey: Constant.DocumentGroupKeys.KEY_GROUP_NAME)
            .setString(group.owner, forKey: Constant.DocumentNoteKeys.KEY_DOC_OWNER)
            .setArray(MutableArrayObject(), forKey: Constant.DocumentGroupKeys.KEY_GROUP_PENDING_MEMBERS)
            .setString(group.type, forKey: Constant.DocumentNoteKeys.KEY_DOC_TYPE)
            .setArray(MutableArrayObject(), forKey: "channels")
        
        let channel: String = "channel:"+mutableGroupDoc.string(forKey: Constant.DocumentNoteKeys.KEY_DOC_OWNER)!
        mutableGroupDoc.array(forKey: "channels")!.addString(channel)
        
        do {
            try database?.saveDocument(mutableGroupDoc)
            print("Document saved successfully with id: \(mutableGroupDoc.id)")
            
            let channel2 = "channel:group"+mutableGroupDoc.id
            mutableGroupDoc.array(forKey: "channels")!.addString(channel2)
            if(group.members.count > 0){
                for member in group.members {
                    mutableGroupDoc.array(forKey: Constant.DocumentGroupKeys.KEY_GROUP_MEMBERS)?.addString(member)
                }
            }
            if(group.pending_members.count > 0){
                for pmember in group.pending_members {
                    mutableGroupDoc.array(forKey: Constant.DocumentGroupKeys.KEY_GROUP_PENDING_MEMBERS)?.addString(pmember)
                }
            }
            mutableGroupDoc.setString(channel2, forKey: Constant.DocumentGroupKeys.KEY_GROUP_SHARED_CHANNEL)
            try database?.saveDocument(mutableGroupDoc)
        } catch {
            fatalError("Error saving document")
        }
    }
    
    public func saveNotebook(cat: NotebookModel){
        let mutableCatDoc = MutableDocument()
            .setNumber(cat.category_id, forKey: Constant.DocumentCategoryKeys.KEY_CATEGORY_ID)
            .setString(cat.color, forKey: Constant.DocumentCategoryKeys.KEY_CATEGORY_COLOR)
            .setString(cat.desc, forKey: Constant.DocumentCategoryKeys.KEY_CATEGORY_DESCRIPTION)
            .setString(cat.name, forKey: Constant.DocumentCategoryKeys.KEY_CATEGORY_NAME)
            .setString(cat.owner, forKey: Constant.DocumentNoteKeys.KEY_DOC_OWNER)
            .setString(cat.type, forKey: Constant.DocumentNoteKeys.KEY_DOC_TYPE)
            .setArray(MutableArrayObject(), forKey: "channels")
        
        let channel: String = "channel:"+mutableCatDoc.string(forKey: Constant.DocumentNoteKeys.KEY_DOC_OWNER)!
        mutableCatDoc.array(forKey: "channels")!.addString(channel)
        
        do {
            try database?.saveDocument(mutableCatDoc)
            print("Document saved successfully with id: \(mutableCatDoc.id)")
        } catch {
            fatalError("Error saving document")
        }
    }
    
    // GET
    public func getNoteList() -> [NoteModel] {
        var notes = [NoteModel]()
        let query = QueryBuilder
            .select(SelectResult.all(), SelectResult.expression(Meta.id))
            .from(DataSource.database(database!))
            .where(Expression.property("type").equalTo(Expression.string("note"))
                .and(Expression.property("archived").equalTo(Expression.boolean(false))
                .and(Expression.property("trashed").equalTo(Expression.boolean(false)))
                )
            )
        
        
        do {
            
            for result in try query.execute() {
                
                let id = result.string(forKey: "id")
                print("Note ids: \(id ?? "")")
                
                if let dict = result.dictionary(forKey: Constant.DefaultStorage.DB_NAME) {
                    print("dict:",dict)
                    let name = dict.string(forKey: Constant.DocumentNoteKeys.KEY_TITLE)
                    let content = dict.string(forKey: Constant.DocumentNoteKeys.KEY_CONTENT)
                    let createdon = dict.int(forKey: Constant.DocumentNoteKeys.KEY_CREATION)
                    let alarm = dict.string(forKey: Constant.DocumentNoteKeys.KEY_REMINDER)
                    let type = dict.string(forKey: Constant.DocumentNoteKeys.KEY_DOC_TYPE)
                    let attachemnt = dict.array(forKey: Constant.DocumentNoteKeys.KEY_ATTACHMENTS)?.toArray()
                    let categoryId = dict.string(forKey: Constant.DocumentNoteKeys.KEY_CATEGORY)
                    let groupId = dict.string(forKey: Constant.DocumentNoteKeys.KEY_GROUP_ID)

//                    var attchementList = [AttachmentModel]()
//                    for data in attachemnt!{
//
//                        let dataObj = data as? [String: Any]
//                        let att = AttachmentModel()
////                        att.attachment_id = data["attachment_id"]
//                        att.uri = "\(dataObj["uri"] ?? "")"
//                        attchementList.append(att)
//                    }
                   

                    let n = NoteModel()
                    n.id = id
                    n.title = name
                    n.category_id = categoryId
                    n.group_id = groupId
                    if (attachemnt?.count)! > 0 {
                        let attche = self.getAttachment(note_id: id!)
                        n.attachments = attche

                    }
                    
                    if( (content != nil) && (content?.count)! > 0){
                        n.content = content
                    }
                    
                    if((alarm != nil) && (alarm?.count)! > 0){
                        n.alarm = alarm
                    }
                    
                    if((type != nil) && (type?.count)! > 0){
                        n.type = type
                    }
                    n.creation = NSNumber(integerLiteral: createdon)
                    
                    
                    notes.append(n)
                }
            }
            
        } catch let err{
            fatalError("Error in getting note list: \(err.localizedDescription)")
        }
        
        return notes
    }
    func getNotelistByNotebook(notebook_id:String) -> [NoteModel] {
        print("notbookId:",notebook_id)
        var notes = [NoteModel]()
        let query = QueryBuilder
            .select(SelectResult.all(), SelectResult.expression(Meta.id))
            .from(DataSource.database(database!))
            .where(Expression.property("type").equalTo(Expression.string("note"))
                .and(Expression.property("category_id").equalTo(Expression.string(notebook_id)))
        )
        do {
            
            for result in try query.execute() {
                
                let id = result.string(forKey: "id")
                print("Note ids: \(id ?? "")")
                
                if let dict = result.dictionary(forKey: Constant.DefaultStorage.DB_NAME) {
                    print("dict:",dict)
                    let name = dict.string(forKey: Constant.DocumentNoteKeys.KEY_TITLE)
                    let content = dict.string(forKey: Constant.DocumentNoteKeys.KEY_CONTENT)
                    let createdon = dict.int(forKey: Constant.DocumentNoteKeys.KEY_CREATION)
                    let alarm = dict.string(forKey: Constant.DocumentNoteKeys.KEY_REMINDER)
                    let type = dict.string(forKey: Constant.DocumentNoteKeys.KEY_DOC_TYPE)
                    let attachemnt = dict.array(forKey: Constant.DocumentNoteKeys.KEY_ATTACHMENTS)?.toArray()
                    let categoryId = dict.string(forKey: Constant.DocumentNoteKeys.KEY_CATEGORY)
                    
                    //                    var attchementList = [AttachmentModel]()
                    //                    for data in attachemnt!{
                    //
                    //                        let dataObj = data as? [String: Any]
                    //                        let att = AttachmentModel()
                    ////                        att.attachment_id = data["attachment_id"]
                    //                        att.uri = "\(dataObj["uri"] ?? "")"
                    //                        attchementList.append(att)
                    //                    }
                    
                    
                    let n = NoteModel()
                    n.id = id
                    n.title = name
                    print("NotbookName:",name)

                    n.category_id = categoryId
                    if (attachemnt?.count)! > 0 {
                        let attche = self.getAttachment(note_id: id!)
                        n.attachments = attche
                        
                    }
                    
                    if( (content != nil) && (content?.count)! > 0){
                        n.content = content
                    }
                    
                    if((alarm != nil) && (alarm?.count)! > 0){
                        n.alarm = alarm
                    }
                    
                    if((type != nil) && (type?.count)! > 0){
                        n.type = type
                    }
                    n.creation = NSNumber(integerLiteral: createdon)
                    
                    
                    notes.append(n)
                }
            }
            
        } catch let err{
            fatalError("Error in getting note list: \(err.localizedDescription)")
        }
        
        // Do as other note list is doing
        return notes
    }
    
    func getNotelistByGroup(group_id:String) -> [NoteModel] {
        var notes = [NoteModel]()
        let query = QueryBuilder
            .select(SelectResult.all(), SelectResult.expression(Meta.id))
            .from(DataSource.database(database!))
            .where(Expression.property("type").equalTo(Expression.string("note"))
                .and(Expression.property("group_id").equalTo(Expression.string(group_id)))
        )
        do {
            
            for result in try query.execute() {
                
                let id = result.string(forKey: "id")
                print("Note ids: \(id ?? "")")
                
                if let dict = result.dictionary(forKey: Constant.DefaultStorage.DB_NAME) {
                    print("dict:",dict)
                    let name = dict.string(forKey: Constant.DocumentNoteKeys.KEY_TITLE)
                    let content = dict.string(forKey: Constant.DocumentNoteKeys.KEY_CONTENT)
                    let createdon = dict.int(forKey: Constant.DocumentNoteKeys.KEY_CREATION)
                    let alarm = dict.string(forKey: Constant.DocumentNoteKeys.KEY_REMINDER)
                    let type = dict.string(forKey: Constant.DocumentNoteKeys.KEY_DOC_TYPE)
                    let attachemnt = dict.array(forKey: Constant.DocumentNoteKeys.KEY_ATTACHMENTS)?.toArray()
                    let categoryId = dict.string(forKey: Constant.DocumentNoteKeys.KEY_CATEGORY)
                    
                    //                    var attchementList = [AttachmentModel]()
                    //                    for data in attachemnt!{
                    //
                    //                        let dataObj = data as? [String: Any]
                    //                        let att = AttachmentModel()
                    ////                        att.attachment_id = data["attachment_id"]
                    //                        att.uri = "\(dataObj["uri"] ?? "")"
                    //                        attchementList.append(att)
                    //                    }
                    
                    
                    let n = NoteModel()
                    n.id = id
                    n.title = name
                    print("group Name:", name)
                    n.category_id = categoryId
                    if (attachemnt?.count)! > 0 {
                        let attche = self.getAttachment(note_id: id!)
                        n.attachments = attche
                        
                    }
                    
                    if( (content != nil) && (content?.count)! > 0){
                        n.content = content
                    }
                    
                    if((alarm != nil) && (alarm?.count)! > 0){
                        n.alarm = alarm
                    }
                    
                    if((type != nil) && (type?.count)! > 0){
                        n.type = type
                    }
                    n.creation = NSNumber(integerLiteral: createdon)
                    
                    
                    notes.append(n)
                }
            }
            
        } catch let err{
            fatalError("Error in getting note list: \(err.localizedDescription)")
        }
        
        // Do as other note list is doing
        return notes

        // Do as other note list is doing
    }
    func getNoteListTrashed() -> [NoteModel] {
        var notes = [NoteModel]()
        let query = QueryBuilder
            .select(SelectResult.all(), SelectResult.expression(Meta.id))
            .from(DataSource.database(database!))
            .where(Expression.property("type").equalTo(Expression.string("note"))
                .and(Expression.property("trashed").equalTo(Expression.boolean(true)))
            )
        
        do {
            
            for result in try query.execute() {
                
                let id = result.string(forKey: "id")
                print("Note ids: \(id ?? "")")
                if let dict = result.dictionary(forKey: Constant.DefaultStorage.DB_NAME) {
                    
                    let name = dict.string(forKey: Constant.DocumentNoteKeys.KEY_TITLE)
                    let content = dict.string(forKey: Constant.DocumentNoteKeys.KEY_CONTENT)
                    let createdon = dict.int(forKey: Constant.DocumentNoteKeys.KEY_CREATION)
                    let alarm = dict.string(forKey: Constant.DocumentNoteKeys.KEY_REMINDER)
                    let type = dict.string(forKey: Constant.DocumentNoteKeys.KEY_DOC_TYPE)
                    
                    let n = NoteModel()
                    n.id = id
                    n.title = name
                    
                    if( (content != nil) && (content?.count)! > 0){
                        n.content = content
                    }
                    
                    if((alarm != nil) && (alarm?.count)! > 0){
                        n.alarm = alarm
                    }
                    
                    if((type != nil) && (type?.count)! > 0){
                        n.type = type
                    }
                    n.creation = NSNumber(integerLiteral: createdon)
                    
                    
                    notes.append(n)
                }
            }
            
        } catch let err{
            fatalError("Error in getting note list: \(err.localizedDescription)")
        }
        
        return notes
    }
    
    func getNoteListArchived() -> [NoteModel] {
        var notes = [NoteModel]()
        let query = QueryBuilder
            .select(SelectResult.all(), SelectResult.expression(Meta.id))
            .from(DataSource.database(database!))
            .where(Expression.property("type").equalTo(Expression.string("note"))
                .and(Expression.property("archived").equalTo(Expression.boolean(true)))
        )
        
        do {
            
            for result in try query.execute() {
                
                let id = result.string(forKey: "id")
                print("Note ids: \(id ?? "")")
                if let dict = result.dictionary(forKey: Constant.DefaultStorage.DB_NAME) {
                    
                    let name = dict.string(forKey: Constant.DocumentNoteKeys.KEY_TITLE)
                    let content = dict.string(forKey: Constant.DocumentNoteKeys.KEY_CONTENT)
                    let createdon = dict.int(forKey: Constant.DocumentNoteKeys.KEY_CREATION)
                    let alarm = dict.string(forKey: Constant.DocumentNoteKeys.KEY_REMINDER)
                    let type = dict.string(forKey: Constant.DocumentNoteKeys.KEY_DOC_TYPE)
                    
                    let n = NoteModel()
                    n.id = id
                    n.title = name
                    
                    if( (content != nil) && (content?.count)! > 0){
                        n.content = content
                    }
                    
                    if((alarm != nil) && (alarm?.count)! > 0){
                        n.alarm = alarm
                    }
                    
                    if((type != nil) && (type?.count)! > 0){
                        n.type = type
                    }
                    n.creation = NSNumber(integerLiteral: createdon)
                    
                    
                    notes.append(n)
                }
            }
            
        } catch let err{
            fatalError("Error in getting note list: \(err.localizedDescription)")
        }
        
        
        return notes
    }
    
    func getNoteCheckList() -> [NoteModel] {
        var notes = [NoteModel]()
        let query = QueryBuilder
            .select(SelectResult.all(), SelectResult.expression(Meta.id))
            .from(DataSource.database(database!))
            .where(Expression.property("type").equalTo(Expression.string("note"))
                .and(Expression.property("checklist").equalTo(Expression.boolean(true)))
        )
        
        do {
            
            for result in try query.execute() {
                
                let id = result.string(forKey: "id")
                print("Note ids: \(id ?? "")")
                if let dict = result.dictionary(forKey: Constant.DefaultStorage.DB_NAME) {
                    
                    let name = dict.string(forKey: Constant.DocumentNoteKeys.KEY_TITLE)
                    let content = dict.string(forKey: Constant.DocumentNoteKeys.KEY_CONTENT)
                    let createdon = dict.int(forKey: Constant.DocumentNoteKeys.KEY_CREATION)
                    let alarm = dict.string(forKey: Constant.DocumentNoteKeys.KEY_REMINDER)
                    let type = dict.string(forKey: Constant.DocumentNoteKeys.KEY_DOC_TYPE)
                    
                    let n = NoteModel()
                    n.id = id
                    n.title = name
                    
                    if( (content != nil) && (content?.count)! > 0){
                        n.content = content
                    }
                    
                    if((alarm != nil) && (alarm?.count)! > 0){
                        n.alarm = alarm
                    }
                    
                    if((type != nil) && (type?.count)! > 0){
                        n.type = type
                    }
                    n.creation = NSNumber(integerLiteral: createdon)
                    
                    
                    notes.append(n)
                }
            }
            
        } catch let err{
            fatalError("Error in getting note list: \(err.localizedDescription)")
        }
        
        
        return notes
    }
    
    
    
    func getNotebook() -> [NotebookModel]{
        var notebooks = [NotebookModel]()
        let query = QueryBuilder
            .select(SelectResult.all(), SelectResult.expression(Meta.id))
            .from(DataSource.database(database!))
            .where(Expression.property("type").equalTo(Expression.string("category")))
        
        do {
            for result in try query.execute() {
                
                let id = result.string(forKey: "id")
                print("Note ids: \(id ?? "")")
                if let dict = result.dictionary(forKey: Constant.DefaultStorage.DB_NAME) {
                    let cat_id = dict.number(forKey: Constant.DocumentCategoryKeys.KEY_CATEGORY_ID)
                    let color = dict.string(forKey: Constant.DocumentCategoryKeys.KEY_CATEGORY_COLOR)
                    let desc = dict.string(forKey: Constant.DocumentCategoryKeys.KEY_CATEGORY_DESCRIPTION)
                    let owner = dict.string(forKey: Constant.DocumentNoteKeys.KEY_DOC_OWNER)
                    let name = dict.string(forKey: Constant.DocumentCategoryKeys.KEY_CATEGORY_NAME)
                    let n = NotebookModel(id: id!, category_id: cat_id!, color: color!, desc: desc!, name: name!, owner: owner!)
                    
                    notebooks.append(n)
                    
                }
            }
            
        } catch let err{
            fatalError("Error in getting note list: \(err.localizedDescription)")
        }
        
        return notebooks
    }
    func getNotebook(notebook_id: String) -> NotebookModel {
        let notebook = NotebookModel()
        let doc = database?.document(withID: notebook_id)
        notebook.color = doc?.toMutable().string(forKey: Constant.DocumentCategoryKeys.KEY_CATEGORY_COLOR)
        notebook.name = doc?.string(forKey: Constant.DocumentCategoryKeys.KEY_CATEGORY_NAME)
        
        return notebook
    }
    func getGroup() -> [GroupModel] {
        var groups = [GroupModel]()
        let query = QueryBuilder
            .select(SelectResult.all(), SelectResult.expression(Meta.id))
            .from(DataSource.database(database!))
            .where(Expression.property("type").equalTo(Expression.string(Constant.DocumentTypes.TYPE_GROUP)))
        
        do {
            for result in try query.execute() {
                
                let id = result.string(forKey: "id")
                print("Group ids: \(id ?? "")")
                if let dict = result.dictionary(forKey: Constant.DefaultStorage.DB_NAME) {
                    let group_id = dict.number(forKey: Constant.DocumentGroupKeys.KEY_GROUP_ID)
                    let color = dict.string(forKey: Constant.DocumentGroupKeys.KEY_GROUP_COLOR)
                    let desc = dict.string(forKey: Constant.DocumentGroupKeys.KEY_GROUP_DESCRIPTION)
                    let owner = dict.string(forKey: Constant.DocumentNoteKeys.KEY_DOC_OWNER)
                    let name = dict.string(forKey: Constant.DocumentGroupKeys.KEY_GROUP_NAME)
                    let shared_channel = dict.string(forKey: Constant.DocumentGroupKeys.KEY_GROUP_SHARED_CHANNEL)
                    let members = dict.array(forKey: Constant.DocumentGroupKeys.KEY_GROUP_MEMBERS)
                    let pending_members = dict.array(forKey: Constant.DocumentGroupKeys.KEY_GROUP_PENDING_MEMBERS)

                    let g = GroupModel(id: id!, color: color!, desc: desc!, group_id: group_id!, group_shared_channel: shared_channel!, members: members?.toArray() as! [String], name: name!, owner: owner!, pending_members: pending_members?.toArray() as! [String])
                    
                    groups.append(g)
                }
            }
            
        } catch let err{
            fatalError("Error in getting note list: \(err.localizedDescription)")
        }
        return groups
    }
    
    func getAttachment(note_id: String) -> [AttachmentModel] {
        var attach = [AttachmentModel]()
        let query = QueryBuilder
            .select(SelectResult.all(), SelectResult.expression(Meta.id))
            .from(DataSource.database(database!))
            .where(Expression.property("type").equalTo(Expression.string(Constant.DocumentTypes.TYPE_ATTACHMENT))
                .and(Expression.property("note_id").equalTo(Expression.string(note_id)))
        )
        do {
            for result in try query.execute() {
                let id = result.string(forKey: "id")
                print("Attachment ids: \(id ?? "")")
                if let dict = result.dictionary(forKey: Constant.DefaultStorage.DB_NAME) {
                    
                    let att_id = dict.number(forKey: Constant.DocumentAttachmentKeys.KEY_ATTACHMENT_ID)
                    let uri = dict.string(forKey: Constant.DocumentAttachmentKeys.KEY_ATTACHMENT_URI)
                    let name = dict.string(forKey: Constant.DocumentAttachmentKeys.KEY_ATTACHMENT_NAME)
                    var bucket = dict.string(forKey: Constant.DocumentAttachmentKeys.KEY_ATTACHMENT_BUCKET_KEY)
                    let size = dict.number(forKey: Constant.DocumentAttachmentKeys.KEY_ATTACHMENT_SIZE)
                    
                    let length = dict.number(forKey: Constant.DocumentAttachmentKeys.KEY_ATTACHMENT_LENGTH)
                    var mime = dict.string(forKey: Constant.DocumentAttachmentKeys.KEY_ATTACHMENT_MIME_TYPE)
                    let sync = dict.boolean(forKey: Constant.DocumentAttachmentKeys.KEY_ATTACHMENT_SYNC)
                    let owner = dict.string(forKey: Constant.DocumentNoteKeys.KEY_DOC_OWNER)
                    if bucket == nil{
                        bucket = ""
                    }
                    if mime == nil{
                        mime = ""
                    }
                    let att = AttachmentModel(attachment_id: att_id!, uri: uri!, name: name!, bucket_key: bucket!, size: 0, length: 0, mime_type: mime!, sync: sync, note_id: note_id, owner: owner!)
                    
                    attach.append(att)
                    
                }
            }
        } catch let err {
            fatalError("Error in getting note's attachment list: \(err.localizedDescription)")
        }
        
        return attach
    }
    
    // UPDATE
    public func updateNote(note_id:String, note: NoteModel){
        let doc = database?.document(withID: note_id)
        let docMutable = doc?.toMutable()
        
        if let content = note.content {
            docMutable?.setString(content, forKey: Constant.DocumentNoteKeys.KEY_CONTENT)
        }
        if let name = note.title {
            docMutable?.setString(name, forKey: Constant.DocumentNoteKeys.KEY_TITLE)
        }
        if let alarm = note.alarm {
            docMutable?.setString(alarm, forKey: Constant.DocumentNoteKeys.KEY_REMINDER)
        }
        if let cat_id = note.category_id {
            docMutable?.setString(cat_id, forKey: Constant.DocumentNoteKeys.KEY_CATEGORY)
        }
        if let group_id = note.group_id {
            docMutable?.setString(group_id, forKey: Constant.DocumentNoteKeys.KEY_GROUP_ID)
        }
        
        docMutable?.setString(note.recurrence_rule, forKey: Constant.DocumentNoteKeys.KEY_RECURRENCE_RULE)
        
        docMutable?.setNumber(note.latitude, forKey: Constant.DocumentNoteKeys.KEY_LATITUDE)
        docMutable?.setNumber(note.longitude, forKey: Constant.DocumentNoteKeys.KEY_LATITUDE)
        
        
        docMutable?.setNumber(Date().timeIntervalSince1970*1000 as NSNumber, forKey: Constant.DocumentNoteKeys.KEY_LAST_MODIFICATION)
        do{
            try database!.saveDocument(docMutable!)
            print("Note Document updated")
        }catch let err{
            fatalError("Error : \(err)")
        }
    }
    
    // update for reminder/alarm
    public func updateReminder(note_id:String, status: Bool, alarmTime: String, recurrance:String){
        let doc = database?.document(withID: note_id)
        let docMutable = doc?.toMutable()
        
        docMutable?.setBoolean(status, forKey: Constant.DocumentNoteKeys.KEY_REMINDER_FIRED)
        docMutable?.setString(alarmTime, forKey: Constant.DocumentNoteKeys.KEY_REMINDER)
        docMutable?.setString(recurrance, forKey: Constant.DocumentNoteKeys.KEY_RECURRENCE_RULE)
        docMutable?.setNumber(Date().timeIntervalSince1970*1000 as NSNumber, forKey: Constant.DocumentNoteKeys.KEY_LAST_MODIFICATION)
        do{
            try database!.saveDocument(docMutable!)
            print("Note Document updated")
        }catch let err{
            fatalError("Error : \(err)")
        }
        
    }
    
    // Change archived status of note
    public func changeArchiveNoteStatus(note_id:String, status: Bool){
        let doc = database?.document(withID: note_id)
        let docMutable = doc?.toMutable()
        
        docMutable?.setBoolean(status, forKey: Constant.DocumentNoteKeys.KEY_ARCHIVED)
        print(Constant.DocumentNoteKeys.KEY_ARCHIVED)
        docMutable?.setNumber(Date().timeIntervalSince1970*1000 as NSNumber, forKey: Constant.DocumentNoteKeys.KEY_LAST_MODIFICATION)
        print(Constant.DocumentNoteKeys.KEY_LAST_MODIFICATION)
        do{
            try database!.saveDocument(docMutable!)
            print("Note Document updated")
        }catch let err{
            fatalError("Error : \(err)")
        }
        
    }
    
    // Change archived status of note
    public func changeTrashNoteStatus(note_id:String, status: Bool){
        let doc = database?.document(withID: note_id)
        let docMutable = doc?.toMutable()
        
        docMutable?.setBoolean(status, forKey: Constant.DocumentNoteKeys.KEY_TRASHED)
        docMutable?.setNumber(Date().timeIntervalSince1970*1000 as NSNumber, forKey: Constant.DocumentNoteKeys.KEY_LAST_MODIFICATION)
        do{
            try database!.saveDocument(docMutable!)
            print("Note Document updated")
        }catch let err{
            fatalError("Error : \(err)")
        }
        
    }
    
    public func updateGroup(group_id:String, group:GroupModel){
        let doc = database?.document(withID: group_id)
        let docMutable = doc?.toMutable()
        
        docMutable?.setString(group.name, forKey: Constant.DocumentGroupKeys.KEY_GROUP_NAME)
        docMutable?.setString(group.desc, forKey: Constant.DocumentGroupKeys.KEY_GROUP_DESCRIPTION)
        docMutable?.setString(group.color, forKey: Constant.DocumentGroupKeys.KEY_GROUP_COLOR)
        
        docMutable?.setNumber(Date().timeIntervalSince1970*1000 as NSNumber, forKey: Constant.DocumentNoteKeys.KEY_LAST_MODIFICATION)
        do{
            try database!.saveDocument(docMutable!)
            print("Group Document updated")
        }catch let err{
            fatalError("Error : \(err)")
        }
    }
    
    
    public func addMemberToGroup(group_id:String, members: [String]){
        let doc = database?.document(withID: group_id)
        let docMutable = doc?.toMutable()
        if(members.count > 1){
            for member in members{
                docMutable?.array(forKey: Constant.DocumentGroupKeys.KEY_GROUP_MEMBERS)?.addString(member)
            }
        }
        
        docMutable?.setNumber(Date().timeIntervalSince1970*1000 as NSNumber, forKey: Constant.DocumentNoteKeys.KEY_LAST_MODIFICATION)
        do{
            try database!.saveDocument(docMutable!)
            print("Group Member Document updated")
        }catch let err{
            fatalError("Error : \(err)")
        }
    }
    
    // remove the given member from the group
    public func removeMemberFromGroup(group_id:String, memberToRemove: String){
        let doc = database?.document(withID: group_id)
        let docMutable = doc?.toMutable()
        
        let members = docMutable?.array(forKey: Constant.DocumentGroupKeys.KEY_GROUP_MEMBERS)?.toArray() as! [String]
        docMutable?.setArray(MutableArrayObject(), forKey: Constant.DocumentGroupKeys.KEY_GROUP_MEMBERS)
        for member in members {
            if(member != memberToRemove){
                docMutable?.array(forKey: Constant.DocumentGroupKeys.KEY_GROUP_MEMBERS)?.addString(member)
            }
        }
        docMutable?.setNumber(Date().timeIntervalSince1970*1000 as NSNumber, forKey: Constant.DocumentNoteKeys.KEY_LAST_MODIFICATION)
        do{
            try database!.saveDocument(docMutable!)
            print("Group Member Document updated")
        }catch let err{
            fatalError("Error : \(err)")
        }
    }
    
    // category has only options to update color, name and description
    public func updateNotebook(notebook_id:String, nb: NotebookModel){
        let doc = database?.document(withID: notebook_id)
        let docMutable = doc?.toMutable()
        
        docMutable?.setString(nb.name, forKey: Constant.DocumentCategoryKeys.KEY_CATEGORY_NAME)
        docMutable?.setString(nb.desc, forKey: Constant.DocumentCategoryKeys.KEY_CATEGORY_DESCRIPTION)
        docMutable?.setString(nb.color, forKey: Constant.DocumentCategoryKeys.KEY_CATEGORY_COLOR)
        
        docMutable?.setNumber(Date().timeIntervalSince1970*1000 as NSNumber, forKey: Constant.DocumentNoteKeys.KEY_LAST_MODIFICATION)
        do{
            try database!.saveDocument(docMutable!)
            print("NoteBook Document updated")
        }catch let err{
            fatalError("Error : \(err)")
        }
    }
    
    // DELETE
    public func deleteNote(){
        
    }
    
    public func deleteAttachment(){
        
    }
    
    public func deleteGroup(){
        
    }
    
    public func deleteNotebook(){
        
    }
    
    public func deleteDB(){
        do{
            try database?.delete()
        }catch let err{
            fatalError("Error : \(err)")
        }
        
    }
    
    
    
    
    
}
