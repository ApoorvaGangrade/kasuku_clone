import UIKit
import SkyFloatingLabelTextField

class LoginTextField: SkyFloatingLabelTextField {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let placeholderAttrs = [NSAttributedStringKey.foregroundColor : UIColor.black]
        let placeholder = NSAttributedString(string: self.placeholder!, attributes: placeholderAttrs)
        self.attributedPlaceholder = placeholder
        self.font = UIFont(name:Constant.ElikaFont.regular, size: 13)!
        self.tintColor = UIColor.black
        self.textColor = UIColor.black
        self.lineColor = UIColor.clear
        self.selectedLineColor = UIColor.clear
        self.selectedTitleColor = UIColor.appThemeColor()
        setPlaceHolderImage(color: UIColor.black)
        
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setBottomHairLine()
    }
    
    
    func setPlaceHolderImage(color : UIColor){
        switch self.tag {
        case 100:
            setImage(text: "",color : color)
        case 101:
            setLeftImage(text: "+Code", color: color)
        default:
            print("Invalid TextField")
        }
    }
    
    func setBottomHairLine(){
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0.0, y: self.frame.height + 10, width: self.frame.width, height: 1.0)
        bottomLine.backgroundColor = UIColor.appThemeColor().cgColor
        self.borderStyle = UITextBorderStyle.none
        self.layer.addSublayer(bottomLine)
    }
    
    func setImage(text : String, color : UIColor){
        var label = UILabel()
        self.rightViewMode = UITextFieldViewMode.always
        label = UILabel(frame: CGRect(x: 0, y: 10, width: 60, height: 26))
        label.font = UIFont.systemFont(ofSize: 12)
        label.adjustsFontSizeToFitWidth = true
        label.textAlignment = .center
        label.textColor = color
        label.text = text
        self.rightView = label
    }
    
    
    let padding = UIEdgeInsets(top: 0, left: 40, bottom: 0, right: 5);
    let blankPadding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 5);
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        if self.tag == 101{
            return UIEdgeInsetsInsetRect(bounds,self.padding)
        }else{
            return UIEdgeInsetsInsetRect(bounds, self.blankPadding)
        }
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        if self.tag == 101{
            return UIEdgeInsetsInsetRect(bounds,self.padding)
        }else{
            return UIEdgeInsetsInsetRect(bounds, self.blankPadding)
        }
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        if self.tag == 101{
            return UIEdgeInsetsInsetRect(bounds,self.padding)
        }else{
            return UIEdgeInsetsInsetRect(bounds, self.blankPadding)
        }
    }
    
    
    func setLeftImage(text : String, color : UIColor, leftTextfield: UITextField? = nil){
        var label = UILabel()
        label = UILabel(frame: CGRect(x: 0, y: 0, width: 30, height: 26))
        label.font = UIFont.systemFont(ofSize: 8)
        label.textAlignment = .left
        label.textColor = color
        label.text = text
        if let _ = leftTextfield{
            leftTextfield?.leftViewMode = UITextFieldViewMode.always
            leftTextfield?.leftView = label
            leftTextfield?.reloadInputViews()
        }else{
            self.leftViewMode = UITextFieldViewMode.always
            self.leftView = label
        }
    }
}

