import UIKit

/**
 *  UIButton Custom Class for ButtonDesign.
 *
 *  @superclass UIButton
 */

 class ButtonDesign: UIButton {
    
   //MARK:- Life Cycle Method
    override func awakeFromNib() {
        super.awakeFromNib()
        // Adjusting Elika Assigned UIFont over the Button.
        let fontSize = self.titleLabel?.font.pointSize
        self.titleLabel!.font = UIFont(name: Constant.ElikaFont.regular, size:fontSize!)
    }
    
    
    
}

