import UIKit

class NoteListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var notebookColor: UILabel!
    @IBOutlet weak var noteTitle: UILabel!
    @IBOutlet weak var noteDesc: UILabel!
    @IBOutlet weak var creationDate: UILabel!
    @IBOutlet weak var alarmview: UIView!
    @IBOutlet weak var alarmLbl: UILabel!
    @IBOutlet weak var imgNotes: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
