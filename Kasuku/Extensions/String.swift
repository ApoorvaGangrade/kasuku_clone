import UIKit

/**
 *  Extension to provide extra capabilities to existing String.
 */
extension String{
    
    //MARK:- Method
    /**
     *  Method for validating Email address.
     *
     * @return Bool value indication validation success of the Email String.
     */
   func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    /**
     *  Method for converting a String Object into UnsafeMutablePointer<Int8>.
     *
     * @return mutableRepresentation which is the UnsafeMutablePointer<Int8> representation of the passed String.
     */
   func toPointer() -> UnsafeMutablePointer<Int8>? {
            let utfRepresentation = (self as NSString).utf8String
            let mutableRepresentation = UnsafeMutablePointer<Int8>(mutating: utfRepresentation)
            return mutableRepresentation
    }
    
    /**
     *  Method for seperating DoorID with the response from the Access Control Manager.
     *
     * @return doorId String indicating the result is corresponding to that ID.
     * @return validation String indicating the lock opening result by the Access Control Manager.
     */
    func seperateDoorID()-> (doorId : String, validation : String){
        let componets = self.components(separatedBy: ",")
        return (componets.first!, componets.last!)
    }
    
}
