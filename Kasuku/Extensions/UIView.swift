import UIKit
/**
 *  Extension to provide extra capabilities to existing UIView.
 */
extension UIView{
    
    /**
     *  Method for configuring card layout.
     *
     */
    func configureCardLayout(){
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 0.35
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        self.layer.masksToBounds = false
    }
}

extension UITextField{
    
    /**
     *  Method for configuring card layout.
     *
     */
    func configureTextFieldLayout(){
        self.layer.shadowRadius = 1.0
        self.layer.shadowOpacity = 0.25
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        self.layer.masksToBounds = false
    }
        func setLeftPaddingPoints(_ amount:CGFloat){
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
            self.leftView = paddingView
            self.leftViewMode = .always
        }
        func setRightPaddingPoints(_ amount:CGFloat) {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
            self.rightView = paddingView
            self.rightViewMode = .always
        }
}

extension UIButton{
    
    /**
     *  Method for configuring card layout.
     *
     */
    func configureButtonLayout(){
        self.layer.shadowRadius = 1.0
        self.layer.shadowOpacity = 0.25
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        self.layer.masksToBounds = false
    }
}
