import UIKit

/**
 *  Extension to provide extra capabilities to existing UITableView.
 */
extension UITableView{
    
    /**
     *  Method for dequeueReusableCell.
     *
     * @param T UITableViewCell
     * @return T UITableViewCell
     */
    func dequeueReusableCell<T:UITableViewCell>(type: T.Type) -> T? {
        var fullName: String = NSStringFromClass(T.self)
        if let range = fullName.range(of:".", options:.backwards, range:nil, locale: nil){
            fullName = fullName.substring(from: range.upperBound)
        }
        return self.dequeueReusableCell(withIdentifier: fullName) as? T
    }
}
