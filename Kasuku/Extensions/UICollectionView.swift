import UIKit

/**
 *  Extension to provide extra capabilities to existing UICollectionView.
 */
extension UICollectionView{
    
    /**
     *  Method for dequeueReusableCell.
     *
     * @param T UICollectionViewCell
     * @return T UICollectionViewCell
     */
    func dequeueReusableCell<T:UICollectionViewCell>(type: T.Type,indexPath:IndexPath) -> T? {
        var fullName: String = NSStringFromClass(T.self)
        if let range = fullName.range(of:".", options:.backwards, range:nil, locale: nil){
            fullName = fullName.substring(from: range.upperBound)
        }
        return self.dequeueReusableCell(withReuseIdentifier: fullName, for:indexPath as IndexPath) as? T
    }
}
