import Foundation
/**
 *  Extension to provide extra capabilities to existing Float.
 */
extension Float {
    
    /**
     *  Method for rounding of a mantisa to specific places.
     *
     * @param places Int
     * @return Int
     */
    func roundTo(places:Int) -> Int {
        let divisor = pow(10.0, Float(places))
        return Int((self * divisor).rounded() / divisor)
    }
}

/**
 *  Extension to provide extra capabilities to existing Int.
 */
extension Int {
    
    /**
     *  Method for converting a positive Int Value to negative.
     *
     * @return Int
     */
    func negative() -> Int {
        let num = "-" + String(self)
        return Int(num)!
    }
}
