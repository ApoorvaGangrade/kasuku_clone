import Foundation
import UIKit

extension UIViewController {
    func goToDatePage(page: Int) {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "OKDatePickerVC") as! OKDatePickerVC
        vc.delegate = self as? OKDateDelegate
        vc.flag = page
        present(vc, animated: true, completion: nil)
}
    
    func goToNotebookPage(page: Int) {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "AddNotebookVC") as! AddNotebookVC
        vc.delegate = self as? KNotebookDelegate
//        vc.flag = page
        present(vc, animated: true, completion: nil)
    }
    func goToGroupPage(page: Int) {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "AddGroupVC") as! AddGroupVC
        vc.delegate = self as? KGroupDelegate
        //        vc.flag = page
        present(vc, animated: true, completion: nil)
    }
    
    func startActivityIndicator(withMsg: String, onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let spIntialView = UIView.init(frame: CGRect(x: onView.frame.size.width/2, y: onView.frame.size.height/2, width: 120, height: 120))
        spIntialView.center = spinnerView.center
        spIntialView.backgroundColor = UIColor.black
        spIntialView.layer.cornerRadius = 10
        spIntialView.layer.masksToBounds = true
        let label = UILabel.init(frame: CGRect(x: 0, y: 85, width: 120, height: 20))
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 14.0)
        label.textAlignment = NSTextAlignment.center
        label.text = withMsg
        let ai = UIActivityIndicatorView.init(frame: CGRect(x: 45, y: 45, width: 30, height: 30))
        ai.activityIndicatorViewStyle = .whiteLarge
        ai.startAnimating()
        // ai.center = spIntialView.center
        // label.center = spIntialView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(spIntialView)
            
            spIntialView.addSubview(ai)
            spIntialView.addSubview(label)
            
            onView.addSubview(spinnerView)
        }
        
    }
    
    func stopActivityIndicator(spinner :UIView) {
        DispatchQueue.main.async {
            spinner.subviews.last?.removeFromSuperview()
        }
    }
    
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
        //        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.backgroundColor = UIColor.black
        
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds = true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 3.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
  
}
extension Date {
    var ticks: UInt64 {
        return UInt64((self.timeIntervalSince1970 + 62_135_596_800) * 10_000_000)
    }
}
extension Notification.Name {
    static let didLoadNoteData = Notification.Name("didLoadNoteData")
}
extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}
extension String {
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}
