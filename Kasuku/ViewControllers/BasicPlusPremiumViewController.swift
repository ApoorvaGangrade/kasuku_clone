//
//  BasicPlusPremiumViewController.swift
//  Kasuku
//
//  Created by Apple on 14/12/19.
//  Copyright © 2019 frestyle36. All rights reserved.
//

import UIKit

class BasicPlusPremiumViewController: UIViewController {
   
    @IBOutlet weak var viewBasic: UIView!
    @IBOutlet weak var viewPlus: UIView!
    @IBOutlet weak var viewPremium: UIView!
    @IBOutlet weak var viewBasicLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewPlusLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnPremiumContraint: NSLayoutConstraint!
   
    @IBOutlet weak var btnBasicOutlet: UIButton!
    @IBOutlet weak var btnPlusOutlet: UIButton!
    @IBOutlet weak var btnPremiumOutlet: UIButton!
    
    @IBOutlet weak var lblBasic: UILabel!
    @IBOutlet weak var lblPlus: UILabel!
    @IBOutlet weak var lblPremium: UILabel!

    @IBOutlet weak var tblBasic: UITableView!
    @IBOutlet weak var tblPlus: UITableView!
    @IBOutlet weak var tblPremium: UITableView!
    

    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lblBasic.isHidden = false
        lblPlus.isHidden = true
        lblPremium.isHidden = true
        lblBasic.backgroundColor = .red
        
        tblBasic.delegate = self
        tblBasic.dataSource = self
        tblPlus.delegate = self
        tblPlus.dataSource = self
        tblPremium.delegate = self
        tblPremium.dataSource = self
    }

}
extension BasicPlusPremiumViewController{
    func showViewBasic() {
              viewBasic.isHidden = false
        viewPlus.isHidden = true
        viewPremium.isHidden = true
          view.endEditing(true)
          UIView.animate(withDuration: 0.5, animations: {() -> Void in
              self.viewBasicLeadingConstraint.constant = 0
              self.view.superview?.layoutIfNeeded()
          })
      }
      func hideViewBasic() {
          UIView.animate(withDuration: 0.1, animations: {() -> Void in
              self.viewBasicLeadingConstraint.constant = 500
              self.view.superview?.layoutIfNeeded()
          })
      }
    func showViewPlus() {
             view.endEditing(true)
               viewBasic.isHidden = true
        viewPremium.isHidden = true
                viewPlus.isHidden = false

             UIView.animate(withDuration: 0.5, animations: {() -> Void in
                 self.viewPlusLeadingConstraint.constant = 0
                 self.view.superview?.layoutIfNeeded()
             })
         }
         func hideViewPlus() {
             UIView.animate(withDuration: 0.1, animations: {() -> Void in
                 self.viewPlusLeadingConstraint.constant = 500
                 self.view.superview?.layoutIfNeeded()
             })
         }
    func showViewPremium() {
                viewBasic.isHidden = true
        viewPremium.isHidden = false
                viewPlus.isHidden = true

             view.endEditing(true)
             UIView.animate(withDuration: 0.5, animations: {() -> Void in
                 self.btnPremiumContraint.constant = 0
                 self.view.superview?.layoutIfNeeded()
             })
         }
         func hideViewPremium() {
             UIView.animate(withDuration: 0.1, animations: {() -> Void in
                 self.btnPremiumContraint.constant = 500
                 self.view.superview?.layoutIfNeeded()
             })
         }
}
extension BasicPlusPremiumViewController{
    @IBAction func btnBackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)

       }
       @IBAction func btnBasicAction(_ sender: Any) {
        lblBasic.isHidden = false
               lblPlus.isHidden = true
               lblPremium.isHidden = true
               lblBasic.backgroundColor = .red
        showViewBasic()
        hideViewPlus()
        hideViewPremium()
        
       }
       @IBAction func btnPlusAction(_ sender: Any) {
        lblBasic.isHidden = true
               lblPlus.isHidden = false
               lblPremium.isHidden = true
               lblPlus.backgroundColor = .red
        showViewPlus()
        hideViewBasic()
        hideViewPremium()
        
       }
       @IBAction func btnPremiumAction(_ sender: Any) {
        lblBasic.isHidden = true
               lblPlus.isHidden = true
               lblPremium.isHidden = false
               lblPremium.backgroundColor = .red
        showViewPremium()
        hideViewBasic()
        hideViewPlus()
        

       }
    @IBAction func btnSelectBasicAction(_ sender: Any) {
    }
    @IBAction func btnSelectPlusAction(_ sender: Any) {
    }
    @IBAction func btnSelectPremiumAction(_ sender: Any) {
    }
}
//MARK: Tableview Delegate Datasource methord
extension BasicPlusPremiumViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblBasic {
          
            if indexPath.row == 0{
                let cell = tblBasic.dequeueReusableCell(withIdentifier: "cell1")!
                          return cell

            }else if indexPath.row == 1{
                let cell = tblBasic.dequeueReusableCell(withIdentifier: "cell2")!
                          return cell

            }else{
                let cell = tblBasic.dequeueReusableCell(withIdentifier: "cell3")!
                          return cell
            }

        }else if tableView == tblPlus{
            
            if indexPath.row == 0{
                let cell = tblPlus.dequeueReusableCell(withIdentifier: "cell1")!
                          return cell

            }else if indexPath.row == 1{
                let cell = tblPlus.dequeueReusableCell(withIdentifier: "cell2")!
                          return cell

            }else{
                let cell = tblPlus.dequeueReusableCell(withIdentifier: "cell3")!
                          return cell
            }

        }else{
            
            if indexPath.row == 0{
                let cell = tblPremium.dequeueReusableCell(withIdentifier: "cell1")!
                          return cell

            }else if indexPath.row == 1{
                let cell = tblPremium.dequeueReusableCell(withIdentifier: "cell2")!
                          return cell

            }else{
                let cell = tblPremium.dequeueReusableCell(withIdentifier: "cell3")!
                          return cell
            }
        }
    }
}
