import UIKit
protocol KHandwritingDelegate{
    func imagePathUrl(filePath: String, img : UIImage)
}
class HandwritingVC: BaseVC {
   
    var delegate: KHandwritingDelegate!

    @IBOutlet weak var drawingView: DrawingView!
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.popView)))
    }
    
    @objc func popView(){
        _ = self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func save(_ sender: Any) {
        let  img = drawingView.getImage()
        self.saveImageDocumentDirectory(imageForSave: img)
        
    }
    
    @IBOutlet weak var saveButtonAction: UIButton!
    
    @IBAction func clear(_ sender: Any) {
        drawingView.clear()
    }
    func saveImageDocumentDirectory(imageForSave: UIImage){
        let fileManager = FileManager.default
        let timeStemp = Date().ticks
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("\(timeStemp).png")
        let image = imageForSave
        print(paths)
        let imageData = UIImageJPEGRepresentation(image, 0.5)
        fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
        self.delegate.imagePathUrl(filePath: paths, img: image)
        self.navigationController?.popViewController(animated: false)

    }
}
