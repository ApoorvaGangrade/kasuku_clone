import UIKit
//import CouchbaseLiteSwift

class TextNoteVC: BaseVC, UITextFieldDelegate {

    @IBOutlet weak var noteDescription: UITextView!
    @IBOutlet weak var noteTitle: UITextField!
    @IBOutlet weak var infoBtn: UIButton!
    @IBOutlet weak var alarmBtn: UIButton!
    @IBOutlet weak var tagButton: UIButton!
    @IBOutlet weak var folderIconBtn: UIButton!
    @IBOutlet weak var folderName: UILabel!
    @IBOutlet weak var attachmentBtn: UIButton!
    @IBOutlet weak var alarmTime: UITextField!
    
    private var datepicker : UIDatePicker? = nil
    private var reminder = String()
    
    
    @IBAction func btnDonePressed(_ sender: Any) {
        alertWithCancelCompletion(cancelTitle: "No", buttonTitles: ["Yes"], title: "Save Note", message: "Do you want to save this note", completion: alertResult)
//
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        noteTitle.delegate = self
        
        self.bindDatePicker()
        
    }
    
    func alertResult(result: Bool ){
        if(result){
            print("You selected : \(result)")
            let title = noteTitle.text
            let desc = noteDescription.text
//            let cat_id = String()
//            let type:String = "text"
//            let owner = "testa@a.com"
//            let groupid = String()
//            let attachment = String()
            
            if(isValid(title: title!, desc: desc! )){
                let note = NoteModel()
                note.id = ""
                note.title = title
                note.content = desc
                note.alarm = reminder
                note.type = "text"
                
                saveNoteToDB(note: note)
            self.navigationController?.popViewController(animated: true)
            }
        }else{
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    func isValid(title:String, desc:String) -> Bool{
        if(title.count == 0){
            return false
        }else if (desc.count == 0){
            return false
        }else{
            return true
        }
        
    }
    
    func bindDatePicker(){
        datepicker = UIDatePicker()
        datepicker?.datePickerMode = .dateAndTime
        datepicker?.addTarget(self, action: #selector(TextNoteVC.datechanged(datePicker:)), for: .valueChanged)
        
        alarmTime.inputView = datepicker
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func menuBtnClicked(_ sender: Any) {
        print("menu button clicked")
    }
    
    @IBAction func infoBtnClicked(_ sender: UIButton) {
        print("info button clicked")
    }
    
    @objc func datechanged(datePicker: UIDatePicker){

        let dateformatter = DateFormatter()
        dateformatter.dateStyle = DateFormatter.Style.short
        dateformatter.timeStyle = DateFormatter.Style.short
        reminder = dateformatter.string(from: (datepicker?.date)!)
        
        self.alarmTime.text = "Reminder at : \(reminder)"
        
    }
    
    @IBAction func alarmBtnClicked(_ sender: UIButton) {
        self.alarmTime.becomeFirstResponder()
        
    }
    
    @IBAction func tagBtnClicked(_ sender: Any) {
        print("tag button clicked")
    }
    
    @IBAction func shareBtnClicked(_ sender: Any) {
            let data = Data()

                   // Write the data into a filepath and return the filepath in NSURL
                   // Change the file-extension to specify the filetype (.txt, .json, .pdf, .png, .jpg, .tiff...)
    //               let fileURL = data.dataToFile(fileName: "nameOfYourFile.extension")
            let fileURL = data.dataToFile(fileName: "UNDER DEVELOPMENT")

                   // Create the Array which includes the files you want to share
                   var filesToShare = [Any]()

                   // Add the path of the file to the Array
                   filesToShare.append(fileURL!)

                   // Make the activityViewContoller which shows the share-view
                   let activityViewController = UIActivityViewController(activityItems: filesToShare, applicationActivities: nil)

                   // Show the share-view
                   self.present(activityViewController, animated: true, completion: nil)
               }
}
