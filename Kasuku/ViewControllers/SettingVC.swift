import UIKit

class SettingVC: BaseVC {

    @IBOutlet weak var userEmailLbl: LabelDesign!
    
    @IBOutlet weak var tableView: UITableView!

    @IBOutlet weak var imgProfile: UIImageView!
  
    @IBOutlet weak var tblAccountInfo: UITableView!
    
    @IBOutlet weak var lblAccName: UILabel!

    @IBOutlet weak var lblAccEmail: UILabel!
   
    @IBOutlet weak var lblDataUses: UILabel!
   
    @IBOutlet weak var viewCamera: UIView!
    
    @IBOutlet weak var viewNotification: UIView!
    
    @IBOutlet weak var viewSupport: UIView!
    @IBOutlet weak var viewFolder: UIView!

    var img = UIImageView()
    var imagePicker =  UIImagePickerController()
    var uiimageView = UIImage()
   var isFromTable = false
  
    var sectionOneDataSource = ["Account Info", "Camera", "Folder", "Notification", "Sync"]
    
    var sectionOneDataSourceImage = [UIImage.init(named:"User_AccountInfo"),UIImage.init(named:"CameraUnselected"),UIImage.init(named:"Folder"),UIImage.init(named:"Notification"),UIImage.init(named:"Sync")]

    var sectionTwoDataSource = ["Support", "Privacy policy"]
    var sectionTwoDataSourceImage = [UIImage.init(named:"Support"),UIImage.init(named:"Support")]
    var sectionThreeDataSource = ["Terms of service"]
    var sectionThreeDataSourceImage = [UIImage.init(named:"Support")]
    
  
    override func viewDidLoad() {
        super.viewDidLoad()
        tblAccountInfo.isHidden = true
        viewCamera.isHidden = true
        viewFolder.isHidden = true
        viewNotification.isHidden = true
        viewSupport.isHidden = true
        
        imgProfile.layer.cornerRadius = imgProfile.frame.width/2
        imgProfile.clipsToBounds = true
        
        userEmailLbl.text = UserDefaults.standard.string(forKey: Constant.DefaultStorage.USER_EMAIL)!
        
        if let img = UserDefaults.standard.string(forKey: Constant.DefaultStorage.USER_PROFILE){
                   //imgUser.image = UIImage.init(named: img)
                   let strImg = Constant.ImagePath + img
                   guard let image = Shared.sharedInstance.imageUrl else {return}
                   imgProfile.sd_setImage(with: URL(string: strImg), placeholderImage: #imageLiteral(resourceName: "user_H"))
                   print(image)
               }else{
                   guard let image = Shared.sharedInstance.imageUrl else {return}
                   imgProfile.sd_setImage(with: URL(string: image), placeholderImage: #imageLiteral(resourceName: "user_H"))
                   print(image)
               }
    
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension SettingVC: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return sectionOneDataSource.count
        }else if section == 1{
            return sectionTwoDataSource.count
        }else{
            return sectionThreeDataSource.count

        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(type: LeftMenuTableCell.self)!
        if indexPath.section == 0{
            cell.lblLeftTitle.text = self.sectionOneDataSource[indexPath.row]
            cell.lblLeftIcon.font = UIFont(name: "elika-5th-oct", size: 15)
            cell.imgIocn.image = sectionOneDataSourceImage[indexPath.row]
            return cell
        }else if indexPath.section == 1 {
            cell.lblLeftTitle.text = self.sectionTwoDataSource[indexPath.row]
            cell.lblLeftIcon.font = UIFont(name: "elika-5th-oct", size: 15)
            cell.imgIocn.image = sectionTwoDataSourceImage[indexPath.row]
            return cell
        }else{
            cell.lblLeftTitle.text = self.sectionThreeDataSource[indexPath.row]
            cell.lblLeftIcon.font = UIFont(name: "elika-5th-oct", size: 15)
            cell.imgIocn.image = sectionThreeDataSourceImage[indexPath.row]
            return cell
        }
    }
}
extension SettingVC:UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            if indexPath.row == 0{
                tblAccountInfo.isHidden = false
                isFromTable = true
            }else if indexPath.row == 1{
                viewCamera.isHidden = false
                isFromTable = true
            }else if indexPath.row == 2{
                viewFolder.isHidden = false
                isFromTable = true
            } else if indexPath.row == 3{
                viewNotification.isHidden = false
                isFromTable  = true
            }
        }else if indexPath.section == 1{
            if indexPath.row == 0{
                viewSupport.isHidden = false
                isFromTable  = true
            }
        }else{
            
        }
    }
}
//MARK: Button Action
extension SettingVC{
    @IBAction func btnMultipleShotAction(_ sender: UIButton) {
        if sender.isSelected{
                sender.setImage(#imageLiteral(resourceName: "check-box-empty"), for: .normal)
                sender.isSelected = false
            }else{
                sender.setImage(#imageLiteral(resourceName: "check_box"), for: .normal)
                sender.isSelected = true
            }
    }
    
    @IBAction func btnSavePhotoAction(_ sender: UIButton) {
        if sender.isSelected{
                sender.setImage(#imageLiteral(resourceName: "check-box-empty"), for: .normal)
                sender.isSelected = false
            }else{
                sender.setImage(#imageLiteral(resourceName: "check_box"), for: .normal)
                sender.isSelected = true
            }
    }
    @IBAction func btnDefaultFolderAction(_ sender: Any) {
    }
    @IBAction func btnSaveFolderAction(_ sender: UIButton) {
        if sender.isSelected{
            sender.setImage(#imageLiteral(resourceName: "check-box-empty"), for: .normal)
            sender.isSelected = false
        }else{
            sender.setImage(#imageLiteral(resourceName: "check_box"), for: .normal)
            sender.isSelected = true
        }
    }
    
    @IBAction func btnVirationAction(_ sender: UIButton) {
        if sender.isSelected{
                sender.setImage(#imageLiteral(resourceName: "check-box-empty"), for: .normal)
                sender.isSelected = false
            }else{
                sender.setImage(#imageLiteral(resourceName: "check_box"), for: .normal)
                sender.isSelected = true
            }
    }
    
    @IBAction func btnReminderDealyAction(_ sender: Any) {
    }
    @IBAction func btnChooseImgAction(_ sender: Any) {
        //   showActionSheet()
    }
    @IBAction func btnBasicAction(_ sender: Any) {
        let cont = self.storyboard!.instantiateViewController(type: BasicPlusPremiumViewController.self)!
        self.present(cont, animated: true, completion: nil)
    }
    @IBAction func btnBackPressed(_ sender: Any) {
        if isFromTable{
            isFromTable = false
            tblAccountInfo.isHidden = true
            viewCamera.isHidden = true
            viewFolder.isHidden = true
            viewNotification.isHidden = true
            viewSupport.isHidden = true
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    @IBAction func btnUpgradeAction(_ sender: Any) {
    }
    
    @IBAction func btnSetPassCodeAction(_ sender: Any) {
    }
    
    @IBAction func switchRequestPassword(_ sender: Any) {
    }
    
    @IBAction func btnChangePassword(_ sender: Any) {
    }
    
    @IBAction func btnSignOut(_ sender: Any) {
//        UserDefaults.standard.set(false, forKey: Constant.DefaultStorage.IS_LOGGED_IN)
//        UserDefaults.standard.set("", forKey: Constant.DefaultStorage.USER_EMAIL)
//        UserDefaults.standard.set("", forKey: Constant.DefaultStorage.USER_NAME)
//        UserDefaults.standard.set("", forKey: Constant.DefaultStorage.USER_COUNTRYNAME)
//        UserDefaults.standard.set("", forKey: Constant.DefaultStorage.USER_NAME)
//        UserDefaults.standard.set("", forKey: Constant.DefaultStorage.USER_CONTACTNUMBER)
//        UserDefaults.standard.set("", forKey: Constant.DefaultStorage.USER_EMAIL)
//        UserDefaults.standard.set("", forKey: Constant.DefaultStorage.USER_PROFILE)
//        UserDefaults.standard.synchronize()
//        Shared.sharedInstance.imageUrl = ""
//        self.view.endEditing(true)
//        let signInVC = self.storyboard?.instantiateViewController(type: SignInVC.self)!
//        self.navigationController?.pushViewController(signInVC!, animated: true)
    logout()
    }
    func logout(){
           UserDefaults.standard.set(false, forKey: Constant.DefaultStorage.IS_LOGGED_IN)
           UserDefaults.standard.set("", forKey: Constant.DefaultStorage.USER_EMAIL)
           UserDefaults.standard.set("", forKey: Constant.DefaultStorage.USER_NAME)
           UserDefaults.standard.set("", forKey: Constant.DefaultStorage.USER_COUNTRYNAME)
           UserDefaults.standard.set("", forKey: Constant.DefaultStorage.USER_NAME)
           UserDefaults.standard.set("", forKey: Constant.DefaultStorage.USER_CONTACTNUMBER)
           UserDefaults.standard.set("", forKey: Constant.DefaultStorage.USER_EMAIL)
           UserDefaults.standard.set("", forKey: Constant.DefaultStorage.USER_PROFILE)
           UserDefaults.standard.synchronize()
           Shared.sharedInstance.imageUrl = ""
           self.view.endEditing(true)
           let signInVC = self.storyboard?.instantiateViewController(type: SignInVC.self)
          // self.navigationController?.pushViewController(signInVC!, animated: true)
        self.present(signInVC!, animated: true, completion: nil)

       }
}



