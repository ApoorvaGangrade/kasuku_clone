import UIKit
import MobileCoreServices
import AVFoundation
import AVKit

protocol ComposeNotesDelegate{
    func loadNotes(noteObj: NotesData)
}
class ComposeNotesVC: BaseVC,OKDateDelegate, KNotebookDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource, AVAudioRecorderDelegate, KGroupDelegate{
    func selectedGroup(groupObj: GroupModel) {
        self.groupId = groupObj.id!
    }
    
    @IBOutlet weak var imgSelected: UIImageView!
    var delegate: ComposeNotesDelegate!
    var notebookID = ""
    var loadedurl = ""
    @IBOutlet weak var txtNoteTitle: UITextField!
    @IBOutlet weak var txtNotesDescription: UITextView!
    
    @IBOutlet weak var lblNotobookColor: UILabel!
    @IBOutlet weak var lblNotebook: UILabel!
    @IBOutlet weak var viewAlarm: UIView!
    @IBOutlet weak var lblAlarm: UILabel!
    var flagForView = 0
    var imageFilePath = ""
    var audioFilePath : URL?
    var videoData :  Data?
    var audioData :  Data?
    
    var groupId = ""
    var selectednoteBookColor = ""
    
    @IBOutlet weak var toptextView: NSLayoutConstraint!
    @IBOutlet weak var recodeBtn: UIButton!
    
    var notesData = NoteModel()
    @IBOutlet weak var mCollectionView: UICollectionView!
    
    var recorder: AGAudioRecorder = AGAudioRecorder(withFileName: "TempFile")
    
    
    var controller = UIImagePickerController()
    let videoFileName = "video.mp4"
    var videoFileNamePath : URL?
    var videoFilePath : String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        toptextView.constant = 0
        
        recodeBtn.layer.cornerRadius = recodeBtn.frame.width/2
        recodeBtn.isHidden = true
        self.viewAlarm.isHidden = true
        if flagForView == 2 {
            openCamera()
        }
        if flagForView == 3 {
            openGallery()
        }
        if flagForView == 4{
            handWritingView()
        }
        if flagForView == 6 {
            self.goToDatePage(page: 100)
        }
        if flagForView == 8 {
            self.openVidioCamera()
        }
        if flagForView == 5 {
            let story = UIStoryboard(name: "Main", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "AudioRecordViewController") as! AudioRecordViewController
            vc.delegate = self as KAudioDelegate
            //            vc.flag = page
            present(vc, animated: true, completion: nil)
            
        }
        if flagForView == 10
        {
            self.setPageDataWithotAttachment(noteObj: notesData)
        }
        if flagForView == 11
        {
            self.setPageDataWithAttachMent(noteObj: notesData, flag: flagForView)
        }
        if flagForView == 12
        {
            self.setPageDataWithAttachMent(noteObj: notesData, flag: flagForView)
        }
        if flagForView == 13
        {
            self.setPageDataWithAttachMent(noteObj: notesData, flag: flagForView)
        }
        // Do any additional setup after loading the view.
    }
    
    func openVidioCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            
            // 2 Present UIImagePickerController to take video
            controller.sourceType = .camera
            controller.mediaTypes = [kUTTypeMovie as String]
            controller.delegate = self
            
            present(controller, animated: true, completion: nil)
        }
        else {
            print("Camera is not available")
        }
    }
    
    @IBAction func openAttachmentAction(_ sender: Any) {
        if flagForView == 8 {
            playVideo()
        }
        if flagForView == 11 || flagForView == 13 {
            downloadVidio(vidioUrl: loadedurl)
            
        }
    }
    func playVideo() {
        if videoFileNamePath == nil{return}
        let player = AVPlayer(url: videoFileNamePath!)
        let playerController = AVPlayerViewController()
        playerController.player = player
        present(playerController, animated: true) {
            player.play()
        }
    }
    
    func setPageDataWithotAttachment(noteObj: NoteModel)  {
        self.txtNoteTitle.text = noteObj.title
        self.txtNotesDescription.text = noteObj.content?.html2String
        
        notebookID = noteObj.id!
        let notebook =  dbHelper.getNotebook(notebook_id: notebookID) as? NotebookModel
        if notebook != nil{
            let colorString = "\(notebook?.color ?? "")"
            
            let color = UIColor(hexString: "#\(notebook?.color ?? "")")
            selectednoteBookColor = colorString
            self.lblNotobookColor.backgroundColor = color
            self.lblNotebook.text = "\(notebook?.name ?? "")"
        }
        
        if noteObj.alarm != ""{
            self.lblAlarm.text = noteObj.alarm
            self.viewAlarm.isHidden = false
        }
        
    }
    func setPageDataWithAttachMent(noteObj: NoteModel, flag: Int)  {
        //        self.imgSelected.image = pickedImage
        //        toptextView.constant = 136
        
        self.txtNoteTitle.text = noteObj.title
        self.txtNotesDescription.text = noteObj.content
        if noteObj.alarm != ""{
            self.lblAlarm.text = noteObj.alarm
            self.viewAlarm.isHidden = false
        }
        let bukketKey = noteObj.attachments[0].bucket_key!
        if bukketKey != ""{
            let url = Constant.ImagePath + noteObj.attachments[0].bucket_key!
            self.loadedurl = url
            let urlString = URL(string: url)
            if flagForView == 13{
                self.imgSelected.downloaded(from: urlString!, contentMode: .scaleAspectFit)
                toptextView.constant = 136
                self.imgSelected.isHidden = false
                
                return
            }
            if flagForView == 11{
                self.imgSelected.image = #imageLiteral(resourceName: "video-player.png")
                toptextView.constant = 136
                self.imgSelected.isHidden = false
            }
            if flagForView == 12{
                self.imgSelected.image = #imageLiteral(resourceName: "music-file.png")
                toptextView.constant = 136
                self.imgSelected.isHidden = false
            }
            
        }
    }
    
    
    func downloadVidio(vidioUrl: String) {
        
        self.startActivityIndicator(withMsg: "Loading..", onView: self.view)
        
        if let audioUrl = URL(string: vidioUrl) {
            
            // then lets create your document folder url
            let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            
            // lets create your destination file url
            var extention = ""
            if flagForView == 11{
                let date = Date().timeIntervalSince1970*1000
                extention = "\(date).mp4"
                extention = "video.mp4"
                
            }
            else{
                let date = Date().timeIntervalSince1970*1000
                //                extention = "\(date).mp3"
                extention = "audio.mp3"
                
            }
            let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent + extention)
            print(destinationUrl)
            
            // to check if it exists before downloading it
            if FileManager.default.fileExists(atPath: destinationUrl.path) {
                print("The file already exists at path")
                playDownloAded(urlString: vidioUrl)
                //                let player = AVPlayer(url: destinationUrl)
                //                let playerController = AVPlayerViewController()
                //                playerController.player = player
                //                present(playerController, animated: true) {
                //                    player.play()
                //                }
                // if the file doesn't exist
            } else {
                
                // you can use NSURLSession.sharedSession to download the data asynchronously
                URLSession.shared.downloadTask(with: audioUrl, completionHandler: { (location, response, error) -> Void in
                    guard let location = location, error == nil else { return }
                    do {
                        // after downloading your file you need to move it to your destination url
                        try FileManager.default.moveItem(at: location, to: destinationUrl)
                        print("File moved to documents folder location:\(location) path: \(destinationUrl)")
                        self.playDownloAded(urlString: vidioUrl)
                    } catch let error as NSError {
                        print(error.localizedDescription)
                    }
                }).resume()
            }
        }
        
        
    }
    
    func playDownloAded(urlString: String)  {
        if let audioUrl = URL(string:urlString) {
            
            // then lets create your document folder url
            let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            
            // lets create your destination file url
            var extention = ""
            if flagForView == 11{
                let date = Date().timeIntervalSince1970*1000
                extention = "\(date).mp4"
                extention = "video"
                
            }
            else{
                let date = Date().timeIntervalSince1970*1000
                extention = "\(date).mp3"
                extention = "audio.mp3"
                
            }
            let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent + extention)
            //let url = Bundle.main.url(forResource: destinationUrl, withExtension: "mp3")!
            self.stopActivityIndicator(spinner: self.view)
            
            do {
                let player =  AVPlayer(url: destinationUrl)
                let playerController = AVPlayerViewController()
                playerController.player = player
                self.present(playerController, animated: true) {
                    player.play()
                }
                
                
            } catch let error {
                print(error.localizedDescription)
            }
        }
        
        
    }
    func handWritingView() {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "HandwritingVC") as! HandwritingVC
        vc.delegate = self as KHandwritingDelegate
        //            vc.flagForView = page
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentCell", for: indexPath as IndexPath) as? AttachmentCell
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.frame.size.width/2 - 10, height:collectionView.frame.size.width/2) //collectionView.frame.size.width/2 - 10
    }
    func showAlertMessage()  {
        let alert = UIAlertController(title: "Empty Title", message: "Title can not be empty. Do you want to exit without saving?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            self.navigationController?.popViewController(animated: true)
        })
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func openGallery(){
        let picker = UIImagePickerController()
        picker.sourceType = .photoLibrary
        picker.delegate = self
        picker.mediaTypes = [kUTTypeImage, kUTTypeMovie] as [String]
        self.present(picker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            // imageViewPic.contentMode = .scaleToFill
            
            
            self.saveImageDocumentDirectory(imageForSave: pickedImage)
            self.imgSelected.image = pickedImage
            toptextView.constant = 136
            picker.dismiss(animated: true, completion: nil)
            return
            
        }
        if flagForView == 8 {
            guard let selectedVideo = info[UIImagePickerControllerMediaURL] as? URL else {
                return
            }
            
            // Save video to the main photo album
            let selectorToCall = #selector(ComposeNotesVC.videoSaved(_:didFinishSavingWithError:context:))
            
            // 2
            UISaveVideoAtPathToSavedPhotosAlbum(selectedVideo.relativePath, self, selectorToCall, nil)
            // Save the video to the app directory
            videoData = try? Data(contentsOf: selectedVideo)
            let paths = NSSearchPathForDirectoriesInDomains(
                FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
            let documentsDirectory: URL = URL(fileURLWithPath: paths[0])
            let dataPath = documentsDirectory.appendingPathComponent(videoFileName)
            try! videoData?.write(to: dataPath, options: [])
            videoFileNamePath = dataPath
            videoFilePath = "\(dataPath)"
            self.imgSelected.image = self.getThumbnailFrom(path: URL(fileURLWithPath: "\(dataPath)"))
            toptextView.constant = 136
            
            
        }else{
            if let mediaType = info[UIImagePickerControllerMediaType]  {
                
                if mediaType as! String == kUTTypeMovie as String {
                    let videoURL = info[UIImagePickerControllerMediaURL] as? URL
                    print("VIDEO URL: \(videoURL!)")
                    self.imageFilePath = "\(videoURL!)"
                    self.imgSelected.image = self.getThumbnailFrom(path: URL(fileURLWithPath: "\(videoURL!)"))
                    toptextView.constant = 136
                    
                    
                }
            }
        }
        
        
        // 3
        //        picker.dismiss(animated: true)
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    @objc func videoSaved(_ video: String, didFinishSavingWithError error: NSError!, context: UnsafeMutableRawPointer){
        if let theError = error {
            print("error saving the video = \(theError)")
        } else {
            DispatchQueue.main.async(execute: { () -> Void in
            })
        }
    }
    
    func getThumbnailFrom(path: URL) -> UIImage? {
        
        do {
            
            let asset = AVURLAsset(url: path , options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
            let thumbnail = UIImage(cgImage: cgImage)
            
            return thumbnail
            
        } catch let error {
            
            print("*** Error generating thumbnail: \(error.localizedDescription)")
            return nil
            
        }
        
    }
    func saveImageDocumentDirectory(imageForSave: UIImage){
        let fileManager = FileManager.default
        let timeStemp = Date().ticks
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("\(timeStemp).png")
        let image = imageForSave
        print(paths)
        let imageData = UIImageJPEGRepresentation(image, 0.5)
        fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
        self.imageFilePath = paths
    }
    
    func prepareNoteWithAttachment(filePath: String, mimeType: String, fileData: Data?, formate: String?){
        let date = Date().timeIntervalSince1970*1000
        let mime = mimeType
        let alarm = lblAlarm.text
        let content = txtNotesDescription.text
        let uri = filePath
        let owner = UserDefaults.standard.object(forKey: Constant.DefaultStorage.USER_ID)
        //        let imageURL = URL(fileURLWithPath: uri)
        //        let image    = UIImage(contentsOfFile: imageURL.path)
        //        let imageData = UIImageJPEGRepresentation(image!, 0.5)
        
        
        let note = NoteModel(id: "", address: "", alarm: lblAlarm.text!, archived: false, checklist: false, content: content!, creation: NSNumber(value: date), last_modification: NSNumber(value: date), group_id: self.groupId, latitude: 0, locked: false, longitude: 0, owner: "\(owner ?? "")", recurrence_rule: "", reminder_fired: false, title: txtNoteTitle.text!, trashed: false, category_id: notebookID )
        
        
        let attach = AttachmentModel(attachment_id: NSNumber(value: date), uri: uri, name: "", bucket_key: "", size: 0, length: 0, mime_type: mime, sync: false, note_id: "", owner: "\(owner ?? "")")
        attach.attachementData = fileData
        attach.formate = formate
        
        var attachments = [AttachmentModel]()
        attachments.append(attach)
        
        saveNoteWithAttachment(note: note, attachments:attachments)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    func vailidateField() {
         var notes = NotesData()
        notes.title = txtNoteTitle.text
        notes.des = txtNotesDescription.text
        if self.imageFilePath != "" {
            notes.type = .image
            notes.imagePath = self.imageFilePath
        }
        if lblAlarm.text != ""{
            notes.alarm = self.lblAlarm.text
        }
        if lblNotebook.text != "Notebook" {
            notes.noteBook?.color = self.selectednoteBookColor
            notes.noteBook?.titleNotebook = lblNotebook.text
        }
        let owner = UserDefaults.standard.object(forKey: Constant.DefaultStorage.USER_ID)
        
        notes.creationDate = KUtils.getCurrentDate()
        let noteObj = NoteModel()
        noteObj.title = notes.title
        noteObj.alarm = notes.alarm
        noteObj.content = notes.des
        noteObj.category_id = notebookID
        noteObj.group_id = self.groupId
        noteObj.owner = "\(owner ?? "")"
        print(notes.creationDate!)
       noteObj.dateCreate = notes.creationDate!
        saveNoteToDB(note: noteObj)
        
        self.delegate.loadNotes(noteObj: notes)
        //        if txtNotesDescription.text.isEmpty {
        //            KUtils.showOKAlert(withTitle: "", message: "")
        //        }
    }
    
    
    @IBAction func backButtonAction(_ sender: Any) {
        if  (txtNoteTitle.text?.isEmpty)!{
            showAlertMessage()
            return
        }
        if flagForView == 0{
            vailidateField()
            self.navigationController?.popViewController(animated: true)
            return
            
        }
        if flagForView == 2  || flagForView == 3  || flagForView == 4 {
            let imageURL = URL(fileURLWithPath: self.imageFilePath)
            let image    = UIImage(contentsOfFile: imageURL.path)
            let imageData = UIImageJPEGRepresentation(image!, 0.5)
            
            prepareNoteWithAttachment(filePath: self.imageFilePath, mimeType: "image/jpg", fileData: imageData, formate: ".png")
            // self.navigationController?.popViewController(animated: true)
            return
        }
        if flagForView == 8 {
            prepareNoteWithAttachment(filePath: self.videoFilePath!, mimeType: "video/mp4", fileData: videoData, formate: ".mp4")
            // self.navigationController?.popViewController(animated: true)
            return
            
        }
        
        if flagForView == 5 {
            prepareNoteWithAttachment(filePath: "\(String(describing: self.audioFilePath))", mimeType: "audio/amr", fileData: audioData, formate: ".amr")
            // self.navigationController?.popViewController(animated: true)
            return
            
        }
        if flagForView == 11  || flagForView == 12 || flagForView == 13{
            self.navigationController?.popViewController(animated: true)
        }
        if flagForView == 10{
            self.navigationController?.popViewController(animated: true)
            return
            
        }
        self.navigationController?.popViewController(animated: true)
        
        //  vailidateField()
        //  self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func attachementButtonAction(_ sender: Any) {
    }
    
    @IBAction func notbookButtonAction(_ sender: Any) {
        self.goToNotebookPage(page: 1)
    }
    
    @IBAction func chooseTagAction(_ sender: Any) {
        
    }
    @IBAction func shareButtonAction(_ sender: Any) {
        if flagForView == 5 {

        let url: [Any] = [videoFileNamePath!]
        let avc = UIActivityViewController(activityItems: url, applicationActivities: nil)
        self.present(avc, animated: true)
        }
//        let data = Data()
//
//
//        let fileURL = data.dataToFile(fileName: "UNDER DEVELOPMENT")
//
//               // Create the Array which includes the files you want to share
//               var filesToShare = [Any]()
//
//               // Add the path of the file to the Array
//               filesToShare.append(fileURL!)
//
//               // Make the activityViewContoller which shows the share-view
//               let activityViewController = UIActivityViewController(activityItems: filesToShare, applicationActivities: nil)
//
//               // Show the share-view
//               self.present(activityViewController, animated: true, completion: nil)
//           }
           /*
           // MARK: - Navigation

           // In a storyboard-based application, you will often want to do a little preparation before navigation
           override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
               // Get the new view controller using segue.destinationViewController.
               // Pass the selected object to the new view controller.
           }
           */
    }
    
    @IBAction func threeDotButtonAction(_ sender: Any) {
        self.goToGroupPage(page: 0)
    }
    
    @IBAction func alarmButtonAction(_ sender: Any) {
        self.goToDatePage(page: 100)
    }
    @IBAction func infoButtonAction(_ sender: Any) {
    }
    
    
    @IBAction func playButtonAction(_ sender: Any) {
        recorder.Play(fileurl: self.audioFilePath!)
    }
    
    func selecteDate(date: String, dateServer: String?, flag: Int) {
        self.lblAlarm.text = date
        self.viewAlarm.isHidden = false
    }
    
    func selecteNotes(notebookObj: NotebookModel) {
        notebookID = notebookObj.id!
        let colorString = "\(notebookObj.color ?? "")"
        let color = UIColor(hexString: "#\(colorString)")
        selectednoteBookColor = colorString
        self.lblNotobookColor.backgroundColor = color
        self.lblNotebook.text = "\(notebookObj.name ?? "")"
    }
    
}
extension ComposeNotesVC: KAudioDelegate{
    func selecteAudio(filePath: URL) {
        toptextView.constant = 136
        recodeBtn.isHidden = false
        imgSelected.isHidden = true
        mCollectionView.isHidden = true
        self.audioFilePath = filePath
        audioData = try? Data(contentsOf: filePath)
        
        
    }
}
extension ComposeNotesVC: KHandwritingDelegate{
    func imagePathUrl(filePath: String, img : UIImage)
    {
        toptextView.constant = 136
        recodeBtn.isHidden = true
        imgSelected.isHidden = false
        mCollectionView.isHidden = true
        self.imageFilePath = filePath
        imgSelected.image = img
        
    }
    
    
}
extension Data {

 func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory as NSString
    }
    
    func dataToFile(fileName: String) -> NSURL? {

        // Make a constant from the data
        let data = self

        // Make the file path (with the filename) where the file will be loacated after it is created
        let filePath = getDocumentsDirectory().appendingPathComponent(fileName)

        do {
            // Write the file from data into the filepath (if there will be an error, the code jumps to the catch block below)
            try data.write(to: URL(fileURLWithPath: filePath))

            // Returns the URL where the new file is located in NSURL
            return NSURL(fileURLWithPath: filePath)

        } catch {
            // Prints the localized description of the error from the do block
            print("Error writing the file: \(error.localizedDescription)")
        }

        // Returns nil if there was an error in the do-catch -block
        return nil

    }

}
