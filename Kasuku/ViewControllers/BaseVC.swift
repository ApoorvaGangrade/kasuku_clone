import UIKit
import AVFoundation
import CoreData
import AVKit
import AVFoundation
import CouchbaseLiteSwift
//Module compiled with Swift 5.0.1 cannot be imported by the Swift 5.1.2 compiler: /Volumes/macOS/Users/apple/Documents/Apoorva/Chaaya ma'am project/Project/kasuku/kasuku_old/Pods/CouchbaseLite-Swift/iOS/CouchbaseLiteSwift.framework/Modules/CouchbaseLiteSwift.swiftmodule/x86_64.swiftmodule
/**
 *  UIViewController Custom Class (BaseVC) for Common UIViewController Properties.
 *
 *  @superclass UIViewController
 */
class BaseVC: UIViewController {

    //MARK:- Properties
    let indicator = UIActivityIndicatorView()
    var player: AVAudioPlayer?
    var database: Database? = nil
    let dbHelper = DbHelper()
    
    //MARK:- Supported Interface Orientation Methods
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        return .portrait
    }
    
    override var shouldAutorotate: Bool{
        return false
    }
    
    //MARK:- UIStatusBar Style Preference Methods
    override var prefersStatusBarHidden: Bool{
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    //MARK:- Method
    /**
     *
     *  Method for include activity indicator.
     *
     * @param sender UIViewController
     */
    func includeIndicator(sender : UIViewController){
        AppDelegate.sharedInstance().window?.addSubview(indicator)
        indicator.center = sender.view.center
        sender.view.isUserInteractionEnabled = false
        indicator.bringSubview(toFront: sender.view)
        indicator.startAnimating()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        indicator.center = self.view.center
    }
    
    //MARK:- Method
    /**
     *
     *  Method for include activity indicator.
     *
     */
    func removeIndicator(sender : UIViewController){
      sender.view.isUserInteractionEnabled = true
      indicator.stopAnimating()
    }
    
    
    
    /**
     *
     *  Method for customizing activity indicator.
     *
     */
    func customizeIndicator(){
        indicator.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
        indicator.layer.cornerRadius = 5.0
        indicator.clipsToBounds = true
        indicator.center = self.view.center
        indicator.backgroundColor = UIColor.loaderBackground()
        indicator.color = UIColor.elikaPrimaryColor()
        let transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        indicator.transform = transform
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    /*
     initalize couchdb lite
 */
    func initalizeDatabase(){
        let email = UserDefaults.standard.string(forKey: Constant.DefaultStorage.USER_EMAIL)
        if(email != nil){
            dbHelper.initDb(uname: UserDefaults.standard.string(forKey: Constant.DefaultStorage.USER_EMAIL)!, upass: UserDefaults.standard.string(forKey: Constant.DefaultStorage.USER_CREATION)!)
        }
    }
    func syncToDataBase(){
        
        dbHelper.sync()
    }
    func saveNoteWithAttachment(note: NoteModel, attachments: [AttachmentModel]){
        dbHelper.saveNoteWithAttachment(note: note, attachs: attachments)
    }
    
    func saveNoteToDB(note: NoteModel ){
        dbHelper.saveNote(note: note)
    }
    
    func saveCategoryToDB(notebook: NotebookModel){
        dbHelper.saveNotebook(cat: notebook)
    }
    
    func saveAttachmentToDB(attach: AttachmentModel){
        dbHelper.saveAttachment(attachment: attach, note_id: "")
    }
    
    func saveGroupToDB()  {
        dbHelper.getNotebook()
    }
    
    func removeDB(){
        dbHelper.deleteDB()
    }
    
    func getNotebookFromDB() -> [NotebookModel]{
       return dbHelper.getNotebook()
    }
    
    func deleteAllDoc(){
        
    }
    
    func getNoteList() -> [NoteModel] {
        return dbHelper.getNoteList()
    }
    func getArchiveNoteList() -> [NoteModel] {
        
        return dbHelper.getNoteListArchived()
    }
    func getTrashNoteList() -> [NoteModel] {
        
        return dbHelper.getNoteListTrashed()
    }
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
    // Do any additional setup after loading the view.
        customizeIndicator()
        initalizeDatabase()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /**
     *  Method for Alert methods with completions and Textfields
     .
     *
     * @param cancelTitle String?
     * @param buttonTitles [String]
     * @param title String
     * @param message String
     * @escaping completion Bool
     */
    func alertWithCompletion(cancelTitle:String?,buttonTitles:[String],title:String,message:String,completion:@escaping (Bool)->()){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let setting = UIAlertAction(title: buttonTitles.first!, style: UIAlertActionStyle.default) {
            UIAlertAction in
            completion(true)
        }
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) {
            UIAlertAction in
            completion(false)
        }
        alertController.setValue(attributedText(text: title), forKey: "attributedTitle")
        alertController.addAction(cancel)
        alertController.addAction(setting)
        self.present(alertController, animated: true, completion: nil)
    }

    
    func alertWithSingleCompletition(cancelTitle:String?,buttonTitles:[String],title:String,message:String,completion:@escaping (Bool)->()){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let setting = UIAlertAction(title: buttonTitles.first!, style: UIAlertActionStyle.default) {
            UIAlertAction in
            alertController.dismiss(animated: true, completion: nil)
            completion(true)
        }
        alertController.setValue(attributedText(text: title), forKey: "attributedTitle")
        alertController.addAction(setting)
        self.present(alertController, animated: true, completion: nil)
        completion(false)
    }

    
    
    
    func attributedText(text : String) -> NSMutableAttributedString{
        var mutableString = NSMutableAttributedString()
        mutableString = NSMutableAttributedString(string: text as String, attributes: [NSAttributedStringKey.font:UIFont(name: Constant.ElikaFont.regular, size: 18.0)!])
        mutableString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.appThemeColor(), range: NSRange(location:0,length:text.count))
        return mutableString
    }
    
    func showAlert(cancelTitle:String?,buttonTitles:[String],title:String,message:String){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let action = UIAlertAction(title: buttonTitles.first!, style: UIAlertActionStyle.default) {
            UIAlertAction in
        }
        alertController.setValue(attributedText(text: title), forKey: "attributedTitle")
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func alertWithCancelCompletion(cancelTitle:String?,buttonTitles:[String],title:String,message:String,completion:@escaping (Bool)->()){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title:buttonTitles.last!, style: UIAlertActionStyle.default) {
            UIAlertAction in
            completion(true)
            
        }
        let settings = UIAlertAction(title: cancelTitle, style: UIAlertActionStyle.default) {
            UIAlertAction in
            completion(false)
        }
        alertController.setValue(attributedText(text: title), forKey: "attributedTitle")
        alertController.addAction(action)
        alertController.addAction(settings)
        self.present(alertController, animated: true, completion: nil)
        
    }
    var alertCustom : UIAlertController!
    func alertWithTextField(textFieldsAndTitle : String?,cancelTitle:String?,numberOfTextFields : UInt,buttonTitles:[String],title:String,message:String,completion:@escaping (UITextField,Bool,UIAlertController)->()){
        
        let alert = UIAlertController(title: textFieldsAndTitle, message: message, preferredStyle: .alert)
        alert.addTextField { (textField) in
            completion(textField,false,alert)
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (_) in
        print("Cancel pressed")
        }))
        alert.addAction(UIAlertAction(title: buttonTitles.first, style: .default, handler: { (_) in
            let textField = alert.textFields![0] // Force unwrapping because we know it exists.
             completion(textField,true,alert)
        }))
        
        alert.actions.last?.isEnabled = false
        alertCustom = alert
        alertCustom.setValue(attributedText(text: title), forKey: "attributedTitle")
        self.present(alertCustom, animated: true, completion: nil)
    }
    
    
    func mutipleChoiceDialog(title : String, message : String ,firstBtnTitle : String,SecondBtnTitle : String,CancelBtnTitle : String,completion:@escaping(Int)->()){
        
        var alertController = UIAlertController()
        if DeviceType.IS_IPAD{
          alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        }else{
          alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        }
        
       
        
        let firstAction = UIAlertAction(title:firstBtnTitle, style: UIAlertActionStyle.default) {
            UIAlertAction in
            alertController.dismiss(animated: true, completion: nil)
            completion(0)
            
        }
        let secondAction = UIAlertAction(title: SecondBtnTitle, style: UIAlertActionStyle.default) {
            UIAlertAction in
            alertController.dismiss(animated: true, completion: nil)
            completion(1)
        }
        
        
        let cancelAction = UIAlertAction(title: CancelBtnTitle, style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            completion(2)
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.setValue(attributedText(text: title), forKey: "attributedTitle")
        alertController.addAction(firstAction)
        alertController.addAction(secondAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
    
    func mutipleChoiceDialogForGender(title : String, message : String ,firstBtnTitle : String,SecondBtnTitle : String,CancelBtnTitle : String,completion:@escaping(Int)->()){
        
        var alertController = UIAlertController()
        if DeviceType.IS_IPAD{
            alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        }else{
            alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        }
        
        
        
        let firstAction = UIAlertAction(title:firstBtnTitle, style: UIAlertActionStyle.default) {
            UIAlertAction in
            alertController.dismiss(animated: true, completion: nil)
            completion(0)
            
        }
        let secondAction = UIAlertAction(title: SecondBtnTitle, style: UIAlertActionStyle.default) {
            UIAlertAction in
            alertController.dismiss(animated: true, completion: nil)
            completion(1)
        }
        
        
        let cancelAction = UIAlertAction(title: CancelBtnTitle, style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            completion(2)
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.setValue(attributedText(text: title), forKey: "attributedTitle")
        alertController.addAction(firstAction)
        alertController.addAction(secondAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }

    
    func chooseWifiType(title : String, message : String, completion:@escaping(String)->()){
       let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let firstAction = UIAlertAction(title:"NONE", style: UIAlertActionStyle.default) {
            UIAlertAction in
            alertController.dismiss(animated: true, completion: nil)
            completion("NONE")
            
        }
        let secondAction = UIAlertAction(title:"WEP", style: UIAlertActionStyle.default) {
            UIAlertAction in
            alertController.dismiss(animated: true, completion: nil)
            completion("WEP")
            
        }
        let thirdAction = UIAlertAction(title:"WPA", style: UIAlertActionStyle.default) {
            UIAlertAction in
            alertController.dismiss(animated: true, completion: nil)
            completion("WPA")
            
        }
        let forthAction = UIAlertAction(title:"WPA2", style: UIAlertActionStyle.default) {
            UIAlertAction in
            alertController.dismiss(animated: true, completion: nil)
            completion("WPA2")
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) {
            UIAlertAction in
            alertController.dismiss(animated: true, completion: nil)
            }
        
        alertController.setValue(attributedText(text: title), forKey: "attributedTitle")
        alertController.addAction(firstAction)
        alertController.addAction(secondAction)
        alertController.addAction(thirdAction)
        alertController.addAction(forthAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    /**
     *  Providing delay with duration and completion
     *
     */
    func delay(duration:CGFloat, completion:@escaping ()->()){
        let when = DispatchTime.now() + DispatchTimeInterval.seconds(Int(duration))
        DispatchQueue.main.asyncAfter(deadline: when) { 
            completion()
        }
    }
    
    func delayMilli(duration:CGFloat, completion:@escaping ()->()){
        let when = DispatchTime.now() + DispatchTimeInterval.milliseconds(Int(duration))
        DispatchQueue.main.asyncAfter(deadline: when) {
            completion()
        }
    }
    
    /**
     *  Providing execution on main thread syn and completion
     *
     */
    func syncMainThread(completion:@escaping()->()){
        DispatchQueue.main.sync{
        completion()
        }
    }
}
