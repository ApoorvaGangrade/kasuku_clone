import UIKit
protocol KAudioDelegate{
    func selecteAudio(filePath: URL)
}

class AudioRecordViewController: UIViewController {
    @IBOutlet weak var recodeBtn: UIButton!
    @IBOutlet weak var playBtn: UIButton!
    var delegate: KAudioDelegate!

    var state: AGAudioRecorderState = .Ready
    
    var recorder: AGAudioRecorder = AGAudioRecorder(withFileName: "TempFile")
    
    @IBOutlet weak var lblTime: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        recodeBtn.layer.cornerRadius = recodeBtn.frame.width/2
       // recodeBtn.setTitle("Recode", for: .normal)
        recodeBtn.setTitle("Record", for: .normal)

        playBtn.setTitle("Play", for: .normal)
        recorder.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func recode(_ sender: UIButton) {
      
        recorder.doRecord()
    }
    
    @IBAction func play(_ sender: UIButton) {
        recorder.doPlay()
    }
    
}

extension AudioRecordViewController: AGAudioRecorderDelegate {
    func agAudioRecorder(_ recorder: AGAudioRecorder, withStates state: AGAudioRecorderState) {
        switch state {
        case .error(let e): debugPrint(e)
        case .Failed(let s): debugPrint(s)
            
        case .Finish:
            self.delegate.selecteAudio(filePath: recorder.fileUrl())
         //   recodeBtn.setTitle("Recode", for: .normal)
            recodeBtn.setTitle("Record", for: .normal)
            self.dismiss(animated: true, completion: nil)
            break
            
        case .Recording:
            recodeBtn.setTitle("Stop", for: .normal)
//            self.delegate.selecteAudio(filePath: recorder.fileUrl())
//            self.dismiss(animated: true, completion: nil)
            break

            
        case .Pause:
            playBtn.setTitle("Pause", for: .normal)
            break
            
//        case .Play:
//            playBtn.setTitle("Play", for: .normal)
            
        case .Ready:
          //  recodeBtn.setTitle("Recode", for: .normal)
            recodeBtn.setTitle("Record", for: .normal)

//            self.dismiss(animated: true, completion: nil)
             break
//            playBtn.setTitle("Play", for: .normal)
//            refreshBtn.setTitle("Refresh", for: .normal)
        case .Play:
            break
        }
        debugPrint(state)
    }
    
    func agAudioRecorder(_ recorder: AGAudioRecorder, currentTime timeInterval: TimeInterval, formattedString: String) {
        debugPrint(formattedString)
        lblTime.text = formattedString
    }
}
