import UIKit

class SharedVC: BaseVC {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func btnLeftMenuTapped(_ sender: UIButton) {
        self.frostedViewController.presentMenuViewController()
    }
    
    /**
     *
     *  Method  for configuring pan gesture.
     *
     */
    @objc func panGestureRecognized(sender : UIPanGestureRecognizer){
        self.frostedViewController.panGestureRecognized(sender)
    }
    
    
    /**
     *
     *  Method for adding UIScreenEdgePanGestureRecorgnizer to Side Menu.
     *
     * @param sender UIViewController
     *
     */
    func addSideGesture(sender : UIViewController){
        let panGesture = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(self.panGestureRecognized(sender:)))
        panGesture.edges = .left
        sender.frostedViewController.limitMenuViewSize = true
        sender.frostedViewController.menuViewSize = CGSize(width:Constant.ScreenDimension.width * 0.78,height:Constant.ScreenDimension.height)
        sender.view.addGestureRecognizer(panGesture)
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addSideGesture(sender : self)
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.layoutIfNeeded()
    }
}

extension SharedVC:UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(type: SharedTableCell.self)!
        return cell
    }
}
extension SharedVC:UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
