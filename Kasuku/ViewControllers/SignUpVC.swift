import UIKit

class SignUpVC: BaseVC {

    @IBOutlet weak var tableView: UITableView!
    fileprivate var viewModel = SignUpViewModel()
    
    
    @IBAction func btnCheckBoxPressed(_ sender: Any) {
        self.alertWithCancelCompletion(cancelTitle: "Cancel", buttonTitles: ["OK"], title: "Terms and conditions", message:
        """
1. THESE TERMS

1.1 What these terms cover.

These are the terms and conditions on which Kasuku Note (also referred to as â€œour platformâ€) is made available to you. This includes all of the services or digital content which are also provided.

1.2 Why you should read them.

Please read these terms carefully before you start using Kasuku Note. These terms tell you who we are, how we will provide Kasuku Note to you and to other users, how you and we may change or stop making Kasuku Note available to you, what to do if there is a problem and other important information. If you think that there is a mistake in these terms, please contact us to discuss.

2. INFORMATION ABOUT US AND HOW TO CONTACT US

2.1 Who we are and how to contact us

[DOMAIN ADDRESS] is a platform operated by Twiga Printers & Stationers Limited (â€œWeâ€). We are registered in Kenya under company number [COMPANY NUMBER]. Our main trading address is [TRADING ADDRESS].

To contact us, please email[help@kasukunote.co.ke] or telephone our customer service line on [NUMBER]].

[NOTE TO TWIGA: PLEASE LET ME KNOW WHAT DETAILS YOU PREFER TO USE HERE.]
2.2    By using our platform you accept these terms:
2.2.1 By using our platform, you confirm that you accept these terms of use and that you agree to comply with them.

2.2.2 If you do not agree to these terms, please do not use our platform.

2.2.3    We recommend that you print or save a copy of these terms for future reference.

2.3    There are other terms that may apply to you.

These terms of use refer to the following additional terms, which also apply to your use of our platform:

2.3.1    Our Privacy Policy [INSERT HYPERLINK], which sets out the terms on which we process any personal data we collect from you, or that you provide to us. By using our platform, you consent to such processing and you warrant that all data provided by you is accurate.

2.3.2    If you purchase goods or services from our platform, there may be different terms and conditions which apply to the sale or supply of such goods or services.

2.4    We may make changes to these terms

We amend these terms from time to time. Every time you wish to use our platform, please check these terms to ensure you understand the terms that apply at that time.

3. USING KASUKU NOTE. SOME RULES.

3.1 We may make changes to our platform

We will always strive to provide you with a better product and/or service. We may update and change any part of our platform from time to time as we may need to make improvements to certain aspects. We will try to give you reasonable notice of any major changes where possible but we hope that you understand that this will not always be possible.

3.2 We may suspend or withdraw our platform

3.2.1 Our platform is made available free of charge â€“ we only ask you to submit a fully completed registration form when you open a Kasuku Note account with us. [NOTE TO TWIGA: IS THIS CORRECT?]

3.2.2 We do not guarantee that our platform, or any content on it, will always be available or be uninterrupted. We may suspend or withdraw or restrict the availability of all or any part of our platform for business and operational reasons. We will try to give you reasonable notice of any suspension or withdrawal where possible.

3.2.3 You are also responsible for ensuring that all persons who access our platform through your internet connection and/or Kasuku Note account are aware of these terms of use and other applicable terms and conditions, and that they comply with them.

3.3 You must keep your account details safe

3.3.1 If you choose, or you are provided with, a user identification code, password or any other piece of information as part of our security procedures, you must treat such information as confidential. You must not disclose it to any third party.

3.3.2 We have the right to disable any user identification code or password, whether chosen by you or allocated by us, at any time, if in our reasonable opinion you have failed to comply with any of the provisions of these terms of use.

3.3.3 If you know or suspect that anyone other than you knows your user identification code or password, you must promptly notify us. Please also take steps to reset your security details as soon as you can.

3.4 How you may use material on our platform

3.4.1 We are the owner or the licensee of all intellectual property rights in our platform, and in the material published on it. Those works are protected by copyright laws and treaties in Kenya and around the world. All such rights are reserved.

3.4.2 You can print off and download content from your own Kasuku Notes account for your personal use where you have used the content and our services through your registered account.

3.4.3 Where you access any part of the shared services, where other users have made available their content to other Kasuku Note users, please respect their intellectual property rights in this content. You should also use it for your own private and personal purposes. The content is not available at any time for commercial use.

3.4.4    You must not modify the paper or digital copies of any materials you have printed off or downloaded in any way which was not created by you, and you must not use any illustrations, photographs, video or audio sequences or any graphics separately from any accompanying text.

3.4.5    If you use, print off, copy or download any part of the content on our platform in breach of these terms of use, your right to use our platform will cease immediately and you must, at our option, return or destroy any copies of the materials you have made. We will terminate all access rights to your Kasuku Notes account.

3.5    User-generated content is not approved by us

Our platform may include information and materials uploaded by other users of the platform (and we encourage this), including community bulletin boards and chat rooms. This information and these materials have not been verified or approved by us. The views expressed by other users on our platform do not represent our views or values.

3.6    Do not rely on information on this platform

3.6.1    The content on our platform is provided for general information only and in order to encourage a safe and inspiring environment to share lessons and notes with other Kasuku Note users. It is not intended to amount to information on which you should rely.

3.6.2    We do not monitor the information but we may from time to time make reasonable efforts to ensure that the content posted on Kasuku Note is safe, not discriminatory or offensive, or in any way illegal. We make no representations, warranties or guarantees, whether express or implied, that the content on our platform is accurate, complete or up to date.[NOTE TO TWIGA: WILL YOU MONITOR?]

3.7    We are not responsible for websites or any apps that we link to

3.7.1    Where our platform contains links to other sites and resources provided by third parties, these links are provided for your information only. Such links should not be interpreted as approval by us of those linked websites or information you may obtain from them. This includes any social media pages.

3.7.2    We have no control over the contents of those sites or resources.

If you wish to complain about information and materials uploaded by other users please contact us on [HYPERLINK TO â€œCONTACT USâ€ DETAILS].

4.    UPLOADING CONTENT TO OUR PLATFORM.

4.1    The Kasuku Rules.

Whenever you make use of a feature that allows you to upload content to our platform, or to make contact with other users of our platform, you must comply with the content standards set out in the Kasuku Note Acceptable Use Rules (the â€œKasuku Rulesâ€) [INSERT HYPERLINK TO RELEVANT SECTION].

4.1.1    You must ensure that you are either the owner of the content that you upload, publish or otherwise share on the platform or that you have the permission of the owner to do so. We will not be responsible or in any way liable in the event that a third party claims that you have infringed their intellectual property rights.

4.1.2    The content that you publish, post or otherwise upload must not be false, discriminatory, offensive or in any way illegal.

4.1.3    You warrant that any such contribution does comply with those standards, and you will be liable to us and indemnify us for any breach of that warranty. This means you will be responsible for any loss or damage we suffer as a result of your breach of warranty.

4.1.4    Any content you upload to our platform will be considered non-confidential and non-proprietary. You retain all of your ownership rights in your content, but you are required to grant us and other users of our platform a limited licence to use, store and copy that content and to distribute and make it available to third parties. We also have the right to disclose your identity to any third party who is claiming that any content posted or uploaded by you to our platform constitutes a violation of their intellectual property rights, or of their right to privacy.

4.1.5    We have the right to remove any posting or any other content that you make on our platform if, in our opinion, your post does not comply with the content standards set out in these terms.

4.1.6    You are solely responsible for securing and backing up your content and any other material that is used or that you create when you use Kasuku Note. This extends to all audio, word, graphic and image files that are stored on your Kasuku Note account.

4.2    Rules about linking to our platform

4.2.1    You must not establish a link in such a way as to suggest any form of association, approval or endorsement on our part where none exists.

4.2.2    Our platform must not be framed on any other site, nor may you create a link to any part of our platform other than the home page.

4.2.3    We reserve the right to withdraw linking permission without notice.

4.2.4    If you wish to link to or make any use of content on our platform, please contact [EMAIL ADDRESS].

5.    OUR RESPONSIBILITY

5.1    Our responsibility for loss or damage suffered by you

5.1.1    We do not exclude or limit in any way our liability to you where it would be unlawful to do so. This includes liability for death or personal injury caused by our negligence or the negligence of our employees, agents or subcontractors and for fraud or fraudulent misrepresentation.

5.1.2    Please note that we only provide our platform for domestic and private use. You agree not to use our platform for any commercial or business purposes, and we have no liability to you for any loss of profit, loss of data, loss of business, business interruption, or loss of business opportunity.

5.1.3    We will not be liable for damage that you could have avoided by following our advice to apply an update offered to you free of charge or for damage that was caused by you failing to correctly follow installation instructions or to have in place the minimum system requirements advised by us.

5.2    We are not responsible for viruses and you must not introduce them

5.2.1    We do not guarantee that our platform will be secure or free from bugs or viruses.

5.2.2    You are responsible for configuring your information technology, computer programmes and platform to access our platform. You should use your own virus protection software.

5.2.3    You must not misuse our platform by knowingly introducing viruses, trojans, worms, logic bombs or other material that is malicious or technologically harmful. You must not attempt to gain unauthorised access to our platform, the server on which our platform is stored or any server, computer or database connected to our platform. You must not attack our platform via a denial-of-service attack or a distributed denial-of service attack. We will report any such breach to the relevant law enforcement authorities and we will co-operate with those authorities by disclosing your identity to them. In the event of such a breach, your right to use our platform will cease immediately.

6.    WHAT IF THERE IS A DISPUTE?

Please note that these terms of use, their subject matter and their formation, are governed by Kenyan law. You and we both agree that the courts of Kenya will have exclusive jurisdiction.
"""
        ) { (tap) in
            if tap{
             self.istermsSelected(status: true)
            }else{
             self.istermsSelected(status: false)
            }
        }
    }
    
    func istermsSelected(status: Bool){
        viewModel.termsAccepted = status
        guard let cell = getCell() else {return}
        if viewModel.termsAccepted{
            cell.btnCheckBoxTerms.setTitle("v", for: .normal)
            cell.btnCheckBoxTerms.setTitleColor(UIColor.appThemeColor(), for: .normal)
        }else{
            cell.btnCheckBoxTerms.setTitle("u", for: .normal)
            cell.btnCheckBoxTerms.setTitleColor(UIColor.lightGray, for: .normal)
        }
    }
    
    func getCell()->SignUpTableCell?{
        guard let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? SignUpTableCell else {return nil}
        return cell
    }
    
    @IBAction func btnSignUp(_ sender: Any) {
        let countryValidation = viewModel.validate()
        if countryValidation.0{
           self.includeIndicator(sender: self)
            viewModel.createUser(country: countryValidation.1, accountCreationSource: "account", token : "") { (success, error) in
                self.removeIndicator(sender: self)
                if success{
                
                    self.alertWithSingleCompletition(cancelTitle: nil, buttonTitles: ["OK"], title: Constant.AlertMessages.congo, message: "Account Created Successfully", completion: { (tap) in
                        if tap{
                        _ = self.navigationController?.popViewController(animated: true)
                        }
                    })
                }else{
                    self.showAlert(cancelTitle: nil, buttonTitles: ["OK"], title: Constant.AlertMessages.alertTitle, message: error!)
                }
            }
        }else{
            self.showAlert(cancelTitle: nil, buttonTitles: ["OK"], title: countryValidation.1, message: "")
        }
    }
    
    @IBAction func btnBackPressed(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView(frame: .zero)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension SignUpVC:UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.tableSection
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.tableRows
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(type: SignUpTableCell.self)!
        cell.viewModel = self.viewModel
        return cell
    }
}

extension SignUpVC:UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.tableHeight
    }
}
