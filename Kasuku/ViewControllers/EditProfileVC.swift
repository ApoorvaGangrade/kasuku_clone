import UIKit
import SwiftyJSON

class EditProfileVC: BaseVC {
    
    @IBOutlet weak var userEmailLbl: LabelDesign!
    @IBOutlet weak var userNameLbL: LabelDesign!
    @IBOutlet weak var btnSave: ButtonDesign!
    
    //Apoorva
    @IBOutlet weak var scrollProfile: UIScrollView!
    @IBOutlet weak var scrollViewInner: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var txtName: LoginTextField!
    @IBOutlet weak var txtEmail: LoginTextField!
    @IBOutlet weak var txtCountry: LoginTextField!
    @IBOutlet weak var viewContactNumber: UIView!
    @IBOutlet weak var viewPasswordBackground: UIView!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var txtOldPassword: LoginTextField!
    @IBOutlet weak var txtNewPassword: LoginTextField!
    @IBOutlet weak var txtConfirmPassword: LoginTextField!
    
    @IBOutlet weak var btnOutletUpdate: UIButton!
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var txtMobileNum: LoginTextField!
    @IBOutlet weak var viewPicker: UIView!
    
    @IBOutlet weak var pickerCountry: UIPickerView!
    
    @IBOutlet weak var pickerViewBottomConstraint: NSLayoutConstraint!
    //Array
    var countrieName : [CountriesModel] = [CountriesModel]()
    var arrCountry = [String]()
    var countrySelected : Country = .none
    var strCode = ""
    var strCountry = ""
    var img = UIImageView()
    var imagePicker =  UIImagePickerController()
    var uiimageView = UIImage()
    var imgData:Data?
}
//MARK: UIView lifecycle
extension EditProfileVC{
    override func viewDidLoad() {
        super.viewDidLoad()
        pickerCountry.delegate = self
        self.btnOutletUpdate.backgroundColor = UIColor.appThemeColor()
        self.btnOutletUpdate.configureButtonLayout()
        imgProfile.layer.borderWidth = 0.0
        imgProfile.layer.masksToBounds = false
        imgProfile.layer.cornerRadius = imgProfile.frame.size.width / 2
        imgProfile.clipsToBounds = true
        
        viewPassword.layer.cornerRadius = 10
        viewPassword.layer.masksToBounds = true
        if let img = UserDefaults.standard.string(forKey: Constant.DefaultStorage.USER_PROFILE){
            if img.contains("https://") {
                guard let image = Shared.sharedInstance.imageUrl else {return}
                imgProfile.sd_setImage(with: URL(string: img), placeholderImage: #imageLiteral(resourceName: "user_H"))
                //                     let url = URL(string: image)
                //                     fetchImage(url)
            }else{
                let strImg = Constant.ImagePath + img
                //                guard let image = Shared.sharedInstance.imageUrl else {return}
                imgProfile.sd_setImage(with: URL(string: strImg), placeholderImage: #imageLiteral(resourceName: "user_H"))
                
                //                let url = URL(string: strImg)
                //                print(url!)
                //                fetchImage(url)
            }
        }else{
            guard let image = Shared.sharedInstance.imageUrl else {return}
            imgProfile.sd_setImage(with: URL(string: image), placeholderImage: #imageLiteral(resourceName: "user_H"))
            let url = URL(string: image)
            fetchImage(url)
        }
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getCountriesFromBundle()
        selectLeftView()
        txtEmail.text = UserDefaults.standard.string(forKey: Constant.DefaultStorage.USER_EMAIL)!
        txtName.text! = UserDefaults.standard.string(forKey: Constant.DefaultStorage.USER_NAME)!
        txtMobileNum.text! = UserDefaults.standard.string(forKey: Constant.DefaultStorage.USER_CONTACTNUMBER)!
        txtEmail.isUserInteractionEnabled = false
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        scrollProfile.isScrollEnabled = true
        scrollProfile.contentSize = CGSize(width: scrollProfile.contentSize.width, height: scrollViewInner.frame.size.height)
        print(scrollProfile.contentSize)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//MARK: Custom Methord
extension EditProfileVC{
    private func fetchImage(_ photoURL: URL?) {
        
        guard let imageURL = photoURL else { return  }
        
        DispatchQueue.global(qos: .userInitiated).async {
            do{
                self.imgData = try Data(contentsOf: imageURL)
                
            }catch{
                print("Unable to load data: \(error)")
            }
        }
    }
    func selectLeftView(){
        for objCountry in countrieName{
            let strCountryName = UserDefaults.standard.string(forKey: Constant.DefaultStorage.USER_COUNTRYNAME)!
            if objCountry.name == strCountryName{
                txtCountry.text! = objCountry.name!
                lblCountryCode.text = "+" + objCountry.code!
            }
        }
    }
    func showPicker() {
        view.endEditing(true)
        UIView.animate(withDuration: 0.5, animations: {() -> Void in
            self.pickerViewBottomConstraint.constant = 0
            self.view.superview?.layoutIfNeeded()
        })
    }
    func hidePicker() {
        UIView.animate(withDuration: 0.5, animations: {() -> Void in
            self.pickerViewBottomConstraint.constant = -500
            self.view.superview?.layoutIfNeeded()
        })
    }
    func validate(){
        
        if txtName.text == "", txtName.text!.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            self.showAlert(cancelTitle: nil, buttonTitles: ["OK"], title: Constant.AlertMessages.alertTitle, message: "Enter First Name")
      
        }else if txtCountry.text == ""{
            self.showAlert(cancelTitle: nil, buttonTitles: ["OK"], title: Constant.AlertMessages.alertTitle, message: "Select Country")
       
        }else if txtMobileNum.text == "",  txtMobileNum.text!.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            self.showAlert(cancelTitle: nil, buttonTitles: ["OK"], title: Constant.AlertMessages.alertTitle, message: "Enter Mobile Number")
       
        }else if txtNewPassword.text != txtConfirmPassword.text {
            
            self.showAlert(cancelTitle: nil, buttonTitles: ["OK"], title: Constant.AlertMessages.alertTitle, message: "Entered Password and Confirm Password Doesn't match")
         
        }else if imgProfile.image == #imageLiteral(resourceName: "user_H") {
            self.showAlert(cancelTitle: nil, buttonTitles: ["OK"], title: Constant.AlertMessages.alertTitle, message: "Select Profile Image")
       
        }else{
            editProfile(email: txtEmail.text!, password: txtOldPassword.text!, name: txtName.text!, contactNumber: txtMobileNum.text!, country: txtCountry.text!, newPassword: txtNewPassword.text!)
        }
    }
}

//MARK: Image Picker
extension EditProfileVC: UIImagePickerControllerDelegate ,UINavigationControllerDelegate{
    //MARK----------Add Picture----------
    func showActionSheet() {
        let alert=UIAlertController(title: "Add Photo!", message: nil, preferredStyle: UIAlertController.Style.alert);
        let addGellory = UIAlertAction(title: "Take Photo", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction) in
            self.openCamera()
        })
        let addPhoto = UIAlertAction(title: "Select from the album", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction) in
            self.openGallary()
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil));
        alert.addAction(addGellory)
        alert.addAction(addPhoto)
        present(alert, animated: true, completion: nil);
    }
    func openGallary() {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
    }
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.delegate = self;
            self .present(imagePicker, animated: true, completion: nil)
        } else
        {
            // Create the alert controller
            let alertController = UIAlertController(title: "Wondamapp", message: "Camera not Available", preferredStyle: .alert)
            // Create the actions
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
                UIAlertAction in
            }
            // Add the actions
            alertController.addAction(cancelAction)
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        let imgae = selectedImage!.resizeWithWidth(width: 700)!
        imgProfile.contentMode = .scaleToFill
        imgData = UIImageJPEGRepresentation(imgae, 0.5) //max value is 1.0 and minimum is 0.0
        let compressedImage = UIImage(data: imgData!)
        imgProfile.image = compressedImage
        print(imgData as Any)
        dismiss(animated: true, completion: nil)
    }
    func  imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated:true, completion: nil) //5
    }
}

//MARK: UIPickerView Delegate
extension EditProfileVC:UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countrieName.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
 
        return  titleForPicker(at: row)
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print(row)
        self.txtCountry.text = titleForPicker(at: row)
        lblCountryCode.text = "+" + codeForPicker(at: row)
        didSelect(at: row)
        
        
    }
    func titleForPicker(at row : Int)->String{
        guard let name = countrieName[row].name else {return ""}
        countrySelected = Country.selected(name)
        return name
    }
    
    func codeForPicker(at row : Int)->String{
        guard let name = countrieName[row].code else {return ""}
        return name
    }
    
    func didSelect(at row : Int){
        guard let name = countrieName[row].name else {return}
        countrySelected = Country.selected(name)
    }
}
//MARK: UITextfeild Delegate
extension EditProfileVC:UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtCountry.isUserInteractionEnabled = false
        showPicker()
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        txtCountry.isUserInteractionEnabled = true
        
    }
}
//MARK: Call WebService
extension EditProfileVC{
    
    func editProfile(email: String, password: String,name: String,contactNumber: String,country: String, newPassword: String){
        self.view.endEditing(true)
        
        //Check for internet Services
        if NetworkManager.sharedInstance.isInternetAvailable(){
            let url : String = Constant.serverURL + Constant.EndPoints.updateProfile
            let parameters = ["email" : email, "password": password, "device_id": Constant.deviceID,"contactNumber": contactNumber,"name": name,"country": country,"newPassword": newPassword]
            let headers : [String : String] = [
                "Content-Type":"multipart/form-data"
            ]
            // Adding activity indicator
            includeIndicator(sender: self)
            //Login API Call
            if  let myImage = imgProfile.image{
                imgData = UIImageJPEGRepresentation(myImage, 0.5) //max value is 1.0 and minimum is 0.0
            }
            NetworkManager.sharedInstance.commonNetworkCallToUploadImage(imageData: imgData!, header: headers, url: url, method: .post, parameters: parameters) { (json, error) in
                print(json as Any)
                if let vJson = json?.dictionaryValue{
                    self.removeIndicator(sender: self)
                    if(vJson["success"]!.boolValue){
                        let data = vJson["data"]!.dictionaryValue
                        
                        let createdAt = data["createdAt"]!.intValue
                        let userId = data["id"]!.intValue
                        let userName = data["name"]!.string
                        let userProfilePic = data["profilePic"]!.string
                        let userCountry = data["country"]!.string
                        let userContactNumber = data["contactNumber"]!.string
                        let userEmail = data["email"]!.string
                        
                        UserDefaults.standard.set(userProfilePic, forKey: Constant.DefaultStorage.USER_PROFILE)
                        UserDefaults.standard.set(userCountry, forKey: Constant.DefaultStorage.USER_COUNTRYNAME)
                        UserDefaults.standard.set(userContactNumber, forKey: Constant.DefaultStorage.USER_CONTACTNUMBER)
                        UserDefaults.standard.set(userId, forKey: Constant.DefaultStorage.USER_ID)
                        UserDefaults.standard.set(userName, forKey: Constant.DefaultStorage.USER_NAME)
                        UserDefaults.standard.set(userEmail, forKey: Constant.DefaultStorage.USER_EMAIL)
                        UserDefaults.standard.set(createdAt, forKey: Constant.DefaultStorage.USER_CREATION)
                        UserDefaults.standard.synchronize()
                        
                        self.showAlert(cancelTitle: nil, buttonTitles: ["OK"], title: "Alert", message: (vJson["message"]?.string)!)
                    }
                }
                
            }
            
        }else{
            //Alert for enabling Internet Services
            self.showAlert(cancelTitle: nil, buttonTitles: ["OK"], title: Constant.AlertMessages.alertTitle, message: Constant.AlertMessages.enableInternet)
        }
    }
}
//MARK:  UIButton Action
extension EditProfileVC{
    
    @IBAction func btnDonePickerAction(_ sender: Any) {
        hidePicker()
    }
    @IBAction func btnDoneAction(_ sender: Any) {
        viewPasswordBackground.isHidden = true
        view.endEditing(true)
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        txtNewPassword.text = ""
        txtOldPassword.text = ""
        txtConfirmPassword.text = ""
        viewPasswordBackground.isHidden = true
    }
    @IBAction func btnUpdateAction(_ sender: Any) {
        
        validate()
    
    }
    @IBAction func btnCHangePassword(_ sender: Any) {
        viewPasswordBackground.isHidden = false
    }
    
    @IBAction func btnProfileAction(_ sender: Any) {
        showActionSheet()
    }
    @IBAction func btnBackPressed(_ sender: Any) {
        if Constant.DefaultStorage.isFromLeftMenu == "0" {
            Constant.DefaultStorage.isFromLeftMenu = "1"
            //   self.navigationController?.popViewController(animated: true)
            
            AppDelegate.sharedInstance().setRoot(name: UserDefaults.standard.value(forKey: Constant.DefaultStorage.USER_NAME) as! String, email: UserDefaults.standard.value(forKey: Constant.DefaultStorage.USER_EMAIL) as! String, createdAt: UserDefaults.standard.value(forKey:Constant.DefaultStorage.USER_CREATION) as! Int, profilePic: UserDefaults.standard.value(forKey: Constant.DefaultStorage.USER_PROFILE) as! String)
            
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func btnSavePressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
//MARK: Country code
extension EditProfileVC{
    func getCountriesFromBundle(){
        if let filePath = Bundle.main.path(forResource: "countries", ofType: "json"), let data = NSData(contentsOfFile: filePath) {
            do {
                let json =  try JSONSerialization.jsonObject(with: data as Data, options: .allowFragments)
                print(json)
                
                let countriesJSON = JSON(json)
                let countriesAvailable = countriesJSON.arrayValue
                if countriesAvailable.count > 0{
                    //  self.countries.removeAll()
                    for cCount in countriesAvailable{
                        print(cCount)
                        print(CountriesModel(json: cCount))
                        self.countrieName.append(CountriesModel(json: cCount))
                    }
                }
            }catch let error {
                print(error)
            }
        }
    }
}
extension UIImage {
    func resizeWithPercent(percentage: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: size.width * percentage, height: size.height * percentage)))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
    func resizeWithWidth(width: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
}
