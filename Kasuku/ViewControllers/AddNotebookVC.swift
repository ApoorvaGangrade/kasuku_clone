import UIKit
import IGColorPicker
protocol KNotebookDelegate{
    func selecteNotes(notebookObj: NotebookModel)
}
class AddNotebookVC: BaseVC, ColorPickerViewDelegate, ColorPickerViewDelegateFlowLayout{
    var delegate: KNotebookDelegate!

    @IBOutlet weak var heightNootbook: NSLayoutConstraint!
    
var notebookList = [NotebookModel]()
    @IBOutlet weak var mTableView: UITableView!
    @IBOutlet weak var selectedColorView: UIView!
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var viewNotebook: UIView!
    @IBOutlet weak var viewAddNoteBook: UIView!
    var selectedColor = ""
    @IBOutlet weak var colorPickerView: ColorPickerView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.heightNootbook.constant = CGFloat(self.notebookList.count * 65 + 120)
        viewAddNoteBook.isHidden = true
        selectedColorView.layer.cornerRadius = selectedColorView.frame.width/2
        
        // Setup colorPickerView
        colorPickerView.delegate = self
        colorPickerView.layoutDelegate = self
        colorPickerView.style = .circle
        colorPickerView.selectionStyle = .check
        colorPickerView.isSelectedColorTappable = false
        colorPickerView.preselectedIndex = colorPickerView.colors.indices.first
        let selectColor = colorPickerView.colors.first
        print("SelectedColor HexString:", selectColor?.toHex(alpha: false) ?? "")
        selectedColor = "\(selectColor?.toHex(alpha: false) ?? "")"
        selectedColorView.backgroundColor = colorPickerView.colors.first
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        getNotbookData()

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func getNotbookData()  {
        self.notebookList.removeAll()
        self.notebookList =  getNotebookFromDB()
        if self.notebookList.count > 0 {
            self.heightNootbook.constant = CGFloat(self.notebookList.count * 65 + 120)
            self.mTableView.reloadData()

        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func selectColorAction(_ sender: Any) {
        self.colorPickerView.isHidden = false
    }
    @IBAction func cancelButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
  }
    
    @IBAction func addNotebookAction(_ sender: Any) {
        guard let title = txtTitle.text else {return}
        if title.isEmpty{
            KUtils.showOKAlert(withTitle: "", message: "Please enter title")
        }
        self.addNoteBook()
        
    }
    
    @IBAction func removeNotes(_ sender: Any) {
    }
    
    @IBAction func addNotesAction(_ sender: Any) {
        viewAddNoteBook.isHidden = false
        viewNotebook.isHidden = true


    }
    
    func addNoteBook()  {
        viewAddNoteBook.isHidden = true
        viewNotebook.isHidden = false
        let obj = NotebookModel()
        obj.name = txtTitle.text
        obj.color = selectedColor
        self.notebookList.append(obj)
        print("Notbook count:", self.notebookList.count)
        self.prepareNoteBook(obj: obj)
        self.heightNootbook.constant = CGFloat(self.notebookList.count * 65 + 100)
        self.mTableView.reloadData()
    }
    func prepareNoteBook(obj: NotebookModel){
        let date = Date().timeIntervalSince1970*1000
        let owner = UserDefaults.standard.object(forKey: Constant.DefaultStorage.USER_ID)
        
        let notebook = NotebookModel(id: "", category_id: NSNumber(value: date), color: obj.color!, desc: "", name: obj.name!, owner: "\(owner ?? "")")
        saveCategoryToDB(notebook: notebook)
    }
    
    // MARK: - ColorPickerViewDelegate
    
    func colorPickerView(_ colorPickerView: ColorPickerView, didSelectItemAt indexPath: IndexPath) {
        self.colorPickerView.isHidden = true
        let selectColor = colorPickerView.colors[indexPath.item] as? UIColor
        print("SelectedColor HexString:", selectColor?.toHex(alpha: false) ?? "")
        selectedColor = "\(selectColor?.toHex(alpha: false) ?? "")"

        self.selectedColorView.backgroundColor = colorPickerView.colors[indexPath.item]
    }
    
    // MARK: - ColorPickerViewDelegateFlowLayout
    
    func colorPickerView(_ colorPickerView: ColorPickerView, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 48, height: 48)
    }
    
    func colorPickerView(_ colorPickerView: ColorPickerView, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 11
    }
    
    func colorPickerView(_ colorPickerView: ColorPickerView, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func colorPickerView(_ colorPickerView: ColorPickerView, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
    }
}
extension AddNotebookVC:UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notebookList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellObject : NotebooksCell = tableView.dequeueReusableCell(withIdentifier: "NotebooksCell")! as! NotebooksCell
        cellObject.lblNotbook.text = "\(self.notebookList[indexPath.row].name ?? "")"
        let colorString = "\(self.notebookList[indexPath.row].color ?? "")"
        let color = UIColor(hexString: "#\(colorString)")

        cellObject.imgNotebook.backgroundColor = color
        cellObject.imgNotebook.layer.cornerRadius = cellObject.imgNotebook.frame.width/2
        return cellObject
    }
}
extension AddNotebookVC:UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UserDefaults.standard.set("True", forKey: "SelectFolder")
        UserDefaults.standard.synchronize()
        self.delegate.selecteNotes(notebookObj: self.notebookList[indexPath.row])
        self.dismiss(animated: true, completion: nil)
    }

}
