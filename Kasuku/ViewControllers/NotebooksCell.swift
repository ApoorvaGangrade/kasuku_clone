import UIKit

class NotebooksCell: UITableViewCell {
    @IBOutlet weak var lblNotbook: UILabel!
    @IBOutlet weak var imgNotebook: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
