import UIKit
import REFrostedViewController
import SDWebImage

class LeftMenuVC: BaseVC {


    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblName: LabelDesign!
    @IBOutlet weak var lblEmail: LabelDesign!
    @IBOutlet weak var imgUser: UIImageView!
   
    @IBOutlet weak var imgAllNotes: UIImageView!
    
    @IBOutlet weak var imgReminder: UIImageView!
    
    @IBOutlet weak var imgTrash: UIImageView!
    
    @IBOutlet weak var imgArchive: UIImageView!
    
    var notebookList = [NotebookModel]()
    
    var MenuList = [MenuData]()
    var groupList = [GroupModel]()

    @IBAction func btnEditPressed(_ sender: Any) {
        let cont = self.storyboard!.instantiateViewController(type: EditProfileVC.self)!
        self.present(cont, animated: true, completion: nil)
    }
    @IBAction func btnSyncAction(_ sender: Any) {
        dbHelper.sync()
    }
  //Apoorva
    @IBOutlet weak var ScrollMenu: UIScrollView!
    
    @IBOutlet weak var viewInerMenu: UIView!
    
    @IBOutlet weak var tbleFOlder: UITableView!
    
    @IBOutlet weak var tblGroup: UITableView!
    
    @IBOutlet weak var reminderHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var trashHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var archiveHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var folderHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var groupHeightContraint: NSLayoutConstraint!
    
    @IBOutlet weak var reminderTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var trashTopContraint: NSLayoutConstraint!
    
    @IBOutlet weak var archiveTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var FolderTopContraint: NSLayoutConstraint!
    
    @IBOutlet weak var groupTopContraint: NSLayoutConstraint!
 
    @IBOutlet weak var exploreTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var btnOutletsAllNotes: UIButton!
    
    @IBOutlet weak var btnOutletReminder: UIButton!
    
    @IBOutlet weak var btnOutletTrash: UIButton!
    
    @IBOutlet weak var btnOutletArchive: UIButton!
    
    @IBOutlet weak var btnOutletExplore: UIButton!
    
    @IBOutlet weak var btnOutletSetting: UIButton!
    
    var arrFolder = [String]()//["One","Two","Three","Four"]
    var arrGroup = [String]()//["FirstGp","SecondGp","ThirdGP","FourthGp"]
    var strFolder = ""
    var strGroup = ""
    override func viewDidAppear(_ animated: Bool) {
             super.viewDidAppear(animated)
            ScrollMenu.isScrollEnabled = true
          print(viewInerMenu.frame.size.height)
          ScrollMenu.contentSize = CGSize(width: ScrollMenu.contentSize.width, height: viewInerMenu.frame.size.height)
          print(ScrollMenu.contentSize)
         }
    
    //Apoorva
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView(frame: .zero)
        print(Shared.sharedInstance.userName)
        lblName.text = Shared.sharedInstance.userName
        lblEmail.text = Shared.sharedInstance.userEmail
        
        if let img = UserDefaults.standard.string(forKey: Constant.DefaultStorage.USER_PROFILE){
            //imgUser.image = UIImage.init(named: img)
            let strImg = Constant.ImagePath + img
            guard let image = Shared.sharedInstance.imageUrl else {return}
            imgUser.sd_setImage(with: URL(string: strImg), placeholderImage: #imageLiteral(resourceName: "user_H"))
            print(image)
        }else{
            guard let image = Shared.sharedInstance.imageUrl else {return}
            imgUser.sd_setImage(with: URL(string: image), placeholderImage: #imageLiteral(resourceName: "user_H"))
            print(image)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        print(Shared.sharedInstance.IsFromAllNotes)
        if Shared.sharedInstance.IsFromAllNotes{
          Shared.sharedInstance.IsFromAllNotes = false
            btnOutletsAllNotes.setTitleColor(#colorLiteral(red: 0.003921568627, green: 0.6470588235, blue: 0.8862745098, alpha: 1), for: .normal)
                 imgAllNotes.image = #imageLiteral(resourceName: "All Notes Active")
                 btnOutletReminder.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                 imgReminder.image = #imageLiteral(resourceName: "alarm_Inactive")
                 btnOutletTrash.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                 imgTrash.image = UIImage.init(named: "Trash")
                 btnOutletArchive.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                 imgArchive.image = #imageLiteral(resourceName: "Archive")
        }

        self.MenuList.removeAll()
        var obj = MenuData()
        obj.title = "All Notes"
        obj.type = MenuType.All
        obj.cellType = 0

        MenuList.append(obj)
        
//        var obj1 = MenuData()
//        obj1.title = "Archieve"
//        obj1.type = MenuType.Archieve
//        obj1.cellType = 0
//
//        MenuList.append(obj1)
//
//        var obj2 = MenuData()
//        obj2.title = "Trash"
//        obj2.type = MenuType.Trash
//        obj2.cellType = 0
//
//        MenuList.append(obj2)
//
//        var obj5 = MenuData()
//        obj5.title = "Notebooks"
//        obj5.type = MenuType.Notebook
//        obj5.cellType = 0
//
//        MenuList.append(obj5)
      //  getNotbookData()
        
        var obj3 = MenuData()
        obj3.title = "EXPLORE KASUKU NOTE"
        obj3.type = MenuType.KASUKUNOTE
        obj3.cellType = 0

        MenuList.append(obj3)
        
        var obj4 = MenuData()
        obj4.title = "SETTINGS"
        obj4.type = MenuType.Settings
        obj4.cellType = 0
        MenuList.append(obj4)
        self.tableView.reloadData()

//Aoorva
        if UserDefaults.standard.value(forKey: "Archive")as? String == "Archive"{
            archiveTopConstraint.constant = 0
            archiveHeightConstraint.constant = 50
            btnOutletArchive.setTitle("Archive", for: .normal)
        }else{
            archiveTopConstraint.constant = 0
            archiveHeightConstraint.constant = 0
            btnOutletArchive.setTitle("", for: .normal)
        }
        if UserDefaults.standard.value(forKey: "Reminder")as? String == "Reminder"{
            reminderTopConstraint.constant = 0
            reminderHeightConstraint.constant = 50
            btnOutletReminder.setTitle("Reminder", for: .normal)

        }else{
            reminderTopConstraint.constant = 0
            reminderHeightConstraint.constant = 0
            btnOutletReminder.setTitle("", for: .normal)

        }
        
        if UserDefaults.standard.value(forKey: "Trash")as? String == "Trash"{
            trashTopContraint.constant = 0
            trashHeightConstraint.constant = 50
            btnOutletTrash.setTitle("Trash", for: .normal)
        }else{
            trashTopContraint.constant = 0
            trashHeightConstraint.constant = 0
            btnOutletTrash.setTitle("", for: .normal)
        }
        if  UserDefaults.standard.value(forKey: "Folder")as? String == "Folder"{
            if arrFolder.count>0{
                strFolder = "Folder"
                folderHeightConstraint.constant = 10
                tbleFOlder.reloadData()
                adjustHeightOfTableView()
            }
        }else{
            FolderTopContraint.constant = 0
            folderHeightConstraint.constant = 0
        }
        if UserDefaults.standard.value(forKey: "Group")as? String == "Group"{
            if arrGroup.count>0{
                strGroup = "Group"
                groupHeightContraint.constant = 10
                tblGroup.reloadData()
                adjustHeightOfTableView()
            }
        }else{
            groupTopContraint.constant = 0
            groupHeightContraint.constant = 0
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        imgUser.layer.cornerRadius = imgUser.frame.width/2
        imgUser.clipsToBounds = true        
    }
    func getNotbookData()  {
        self.notebookList.removeAll()
        self.notebookList =  getNotebookFromDB()
        for item in self.notebookList{
            print("Color:", item.color as Any)
            var obj = MenuData()
            obj.title = item.name
            obj.id = item.id
            obj.type = MenuType.Notebook
            obj.cellType = 1
            obj.color = "#\(item.color ?? "")"
            self.MenuList.append(obj)
        }
       getgroupData()
    }
    func getgroupData()  {
        var obj5 = MenuData()
        obj5.title = "Group"
        obj5.type = MenuType.Group
        obj5.cellType = 0
        
        MenuList.append(obj5)
        self.groupList.removeAll()
        self.groupList =  dbHelper.getGroup()
        for item in self.groupList{
            var obj = MenuData()
            obj.title = item.name
            obj.id = "\(item.group_id)"
            obj.type = MenuType.Group
            obj.cellType = 1
            self.MenuList.append(obj)

        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}



extension LeftMenuVC: UITableViewDataSource{
   
  
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tbleFOlder{
            return arrFolder.count
        }else if tableView == tblGroup{
            return arrGroup.count
        }else{
            return MenuList.count
        }
    }
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 60.0
//    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tbleFOlder{
            let cell = tableView.dequeueReusableCell(type: FolderTableViewCell.self)!
            cell.textLabel?.text = arrFolder[indexPath.row]
            return cell

        }else if tableView == tblGroup{
            let cell = tableView.dequeueReusableCell(type: GroupTableViewCell.self)!
//print(self.MenuList[indexPath.row].title)
            cell.textLabel?.text = arrGroup[indexPath.row]
            return cell
            
        }else{
        let cell = tableView.dequeueReusableCell(type: LeftMenuTableCell.self)!
//        if indexPath.row == Shared.sharedInstance.selectedIndex{
//              cell.lblLeftTitle.textColor = UIColor.appThemeColor()
//              cell.lblLeftIcon.textColor = UIColor.appThemeColor()
//        }else{
//            cell.lblLeftTitle.textColor = UIColor.black
//            cell.lblLeftIcon.textColor = UIColor.black
//        }
        if self.MenuList[indexPath.row].cellType == 0{
            cell.lblLeftTitle.text = self.MenuList[indexPath.row].title
            cell.lblSubItem.isHidden = true
            cell.leadingSepratort.constant = 20
            cell.lblLine.isHidden = true
        }
        else{
            print(MenuList.count)
            cell.lblSubItem.text = self.MenuList[indexPath.row].title
            cell.lblLeftTitle.isHidden = true
            cell.leadingSepratort.constant = 36
            cell.lblSubItem.textColor = UIColor(hexString: "\(self.MenuList[indexPath.row].color ?? "")") 
            cell.lblLine.isHidden = false

        }
//        cell.lblLeftIcon.isHidden = true
        
//        cell.lblLeftIcon.font = UIFont(name: "elika-5th-oct", size: 15)
        return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        adjustHeightOfTableView()
        return UITableViewAutomaticDimension
    }
    func adjustHeightOfTableView(){
        if strFolder == "Folder"{
            let height = tbleFOlder.contentSize.height
            UIView.animate(withDuration: 0.25, animations: {
                self.folderHeightConstraint.constant = height
                self.view.layoutIfNeeded()
            })
        }
        if strGroup == "Group"{
            let height = tblGroup.contentSize.height
            UIView.animate(withDuration: 0.25, animations: {
                self.groupHeightContraint.constant = height
                self.view.layoutIfNeeded()
            })
        }
        
    }
}
extension LeftMenuVC:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         let leftVC = self.storyboard!.instantiateViewController(type: LeftMenuVC.self)!
        self.frostedViewController.hideMenuViewController()
        if let type = self.MenuList[indexPath.row].type{
            if type == .All{
                Shared.sharedInstance.selectedIndex = 0
                
            }
            if type == .Archieve{
                Shared.sharedInstance.selectedIndex = 1
            }
            if type == .Trash{
                Shared.sharedInstance.selectedIndex = 2
            }
            if type == .Notebook{
                Shared.sharedInstance.selectedIndex = 3
                if let cellType = self.MenuList[indexPath.row].cellType, cellType == 1{
                    Shared.sharedInstance.categoryId = self.MenuList[indexPath.row].id!
                }
            }
            if type == .Group{
                Shared.sharedInstance.selectedIndex = 4
                if let cellType = self.MenuList[indexPath.row].cellType, cellType == 1{
                    Shared.sharedInstance.categoryId = self.MenuList[indexPath.row].id!
                }
            }
            if type == .KASUKUNOTE{
                Shared.sharedInstance.selectedIndex = 5
            }
            if type == .Settings{
                Shared.sharedInstance.selectedIndex = 6
            }
            NotificationCenter.default.post(name: .didLoadNoteData, object: nil)

        }
//        if indexPath.row == 0{
//           Shared.sharedInstance.selectedIndex = 0
//        }
//        if indexPath.row == 0{
//        if Shared.sharedInstance.selectedIndex != 0{
//            Shared.sharedInstance.selectedIndex = 0
//            _ = self.navigationController?.popToRootViewController(animated: true)
//        }else{
//            self.frostedViewController.hideMenuViewController()
//        }
//        }
//        else if indexPath.row == 1{
//            self.frostedViewController.hideMenuViewController()
//            if Shared.sharedInstance.selectedIndex != 1{
//                Shared.sharedInstance.selectedIndex = 1
////                let usageVC = self.storyboard?.instantiateViewController(type: NotesbooksVC .self)!
////                let container = REFrostedViewController(contentViewController: usageVC, menuViewController: leftVC)
////                self.navigationController?.pushViewController(container!, animated: true)
//            }
//        }
//        else if indexPath.row == 2{
//            self.frostedViewController.hideMenuViewController()
//            if Shared.sharedInstance.selectedIndex != 2{
//                Shared.sharedInstance.selectedIndex = 2
////                let usageVC = self.storyboard?.instantiateViewController(type: SharedVC .self)!
////                let container = REFrostedViewController(contentViewController: usageVC, menuViewController: leftVC)
////                self.navigationController?.pushViewController(container!, animated: true)
//            }
//        }
//        else if indexPath.row == 3{
//            self.frostedViewController.hideMenuViewController()
//            if Shared.sharedInstance.selectedIndex != 2{
//                Shared.sharedInstance.selectedIndex = 2
////                let usageVC = self.storyboard?.instantiateViewController(type: SharedVC .self)!
////                let container = REFrostedViewController(contentViewController: usageVC, menuViewController: leftVC)
////                self.navigationController?.pushViewController(container!, animated: true)
//            }
//        }
//        else if indexPath.row == 5{
//            Shared.sharedInstance.selectedIndex = indexPath.row
//            let cont = self.storyboard?.instantiateViewController(type: SettingVC.self)!
//            self.present(cont!, animated: true, completion: nil)
//        }
    }
}
extension LeftMenuVC{
    @IBAction func btnEditProfileAction(_ sender: Any) {
       Constant.DefaultStorage.isFromLeftMenu = "0"
//        let story = UIStoryboard(name: "Main", bundle: nil)
//               let vc = story.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
//               self.navigationController?.pushViewController(vc, animated: true)
        let cont = self.storyboard!.instantiateViewController(type: EditProfileVC.self)!
               self.present(cont, animated: true, completion: nil)

     
    }
}
//MARK: Buttion Action
//Apoorva
@available(iOS 10.0, *)
@available(iOS 10.0, *)
extension LeftMenuVC{
    @IBAction func btnAllNotesAction(_ sender: UIButton) {
        btnOutletsAllNotes.setTitleColor(#colorLiteral(red: 0.003921568627, green: 0.6470588235, blue: 0.8862745098, alpha: 1), for: .normal)
        imgAllNotes.image = #imageLiteral(resourceName: "All Notes Active")
        btnOutletReminder.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        imgReminder.image = #imageLiteral(resourceName: "alarm_Inactive")
        btnOutletTrash.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        imgTrash.image = UIImage.init(named: "Trash")
        btnOutletArchive.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        imgArchive.image = #imageLiteral(resourceName: "Archive")
        self.frostedViewController.hideMenuViewController()
        Shared.sharedInstance.selectedIndex = 0
        NotificationCenter.default.post(name: .didLoadNoteData, object: nil)
    }
       
       @IBAction func btnReminderAction(_ sender: Any) {
        btnOutletsAllNotes.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
              imgAllNotes.image = #imageLiteral(resourceName: "All Notes")
              btnOutletReminder.setTitleColor(#colorLiteral(red: 0.003921568627, green: 0.6470588235, blue: 0.8862745098, alpha: 1), for: .normal)
              imgReminder.image = #imageLiteral(resourceName: "alarm_Active")
              btnOutletTrash.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
              imgTrash.image = UIImage.init(named: "Trash")
              btnOutletArchive.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
              imgArchive.image = #imageLiteral(resourceName: "Archive")
       }
       
       @IBAction func btnTrashAction(_ sender: Any) {
        btnOutletsAllNotes.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        imgAllNotes.image = #imageLiteral(resourceName: "All Notes")
        btnOutletReminder.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        imgReminder.image = #imageLiteral(resourceName: "alarm_Inactive")
        btnOutletTrash.setTitleColor(#colorLiteral(red: 0.003921568627, green: 0.6470588235, blue: 0.8862745098, alpha: 1), for: .normal)
        imgTrash.image = #imageLiteral(resourceName: "TrashActive")//UIImage.init(named: "Trash")
        btnOutletArchive.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        imgArchive.image = #imageLiteral(resourceName: "Archive")
           self.frostedViewController.hideMenuViewController()
           Shared.sharedInstance.selectedIndex = 2
           NotificationCenter.default.post(name: .didLoadNoteData, object: nil)

       }
       
       @IBAction func btnArchiveAction(_ sender: Any) {
        btnOutletsAllNotes.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        imgAllNotes.image = #imageLiteral(resourceName: "All Notes")
        btnOutletReminder.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        imgReminder.image = #imageLiteral(resourceName: "alarm_Inactive")
        btnOutletTrash.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        imgTrash.image = UIImage.init(named: "Trash")
        btnOutletArchive.setTitleColor(#colorLiteral(red: 0.003921568627, green: 0.6470588235, blue: 0.8862745098, alpha: 1), for: .normal)
        imgArchive.image = #imageLiteral(resourceName: "ArchiveSelected")
           self.frostedViewController.hideMenuViewController()
           Shared.sharedInstance.selectedIndex = 1
           NotificationCenter.default.post(name: .didLoadNoteData, object: nil)

       }
    @IBAction func btnExplore(_ sender: Any) {
//          Shared.sharedInstance.selectedIndex = 5
//          NotificationCenter.default.post(name: .didLoadNoteData, object: nil)
        if let url = URL(string: "http://www.kasukunote.com/") {
            UIApplication.shared.open(url)
        }
       self.frostedViewController.hideMenuViewController()

      }
      
      @IBAction func btnSetting(_ sender: Any) {
          self.frostedViewController.hideMenuViewController()
          Shared.sharedInstance.selectedIndex = 6
          NotificationCenter.default.post(name: .didLoadNoteData, object: nil)
          
      }
      
}
struct MenuData {
    var title : String?
    var icon : UIImage?
    var id : String?
    var type : MenuType?
    var cellType : Int?
    var color : String?


}

enum MenuType {
    case All
    case Archieve
    case Trash
    case Notebook
    case Group
    case KASUKUNOTE
    case Settings
}
