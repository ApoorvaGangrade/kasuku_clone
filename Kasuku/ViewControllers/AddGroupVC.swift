import UIKit
protocol KGroupDelegate{
    func selectedGroup(groupObj: GroupModel)
}
class AddGroupVC: BaseVC {
    @IBOutlet weak var viewGroup: UIView!
    @IBOutlet weak var viewAddGroup: UIView!

    @IBOutlet weak var mTableView: UITableView!
    
    var delegate : KGroupDelegate!
    @IBOutlet weak var txtGroup: UITextField!
    @IBOutlet weak var heightGroupView: NSLayoutConstraint!
    
    var groupList = [GroupModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.heightGroupView.constant = CGFloat(self.groupList.count * 65 + 120)
        viewAddGroup.isHidden = true
        getgroupData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func AddGroupAction(_ sender: Any) {
        viewAddGroup.isHidden = false
        viewGroup.isHidden = true

    }
    
    @IBAction func removegroupAction(_ sender: Any) {
    }
    @IBAction func cancelButtonAction(_ sender: Any) {
          self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func AddGroupToDB(_ sender: Any) {
        guard let title = txtGroup.text else {return}
        if title.isEmpty{
            KUtils.showOKAlert(withTitle: "", message: "Please enter title")
        }
        addgroup()
    }
    
    func getgroupData()  {
        self.groupList.removeAll()
        self.groupList =  dbHelper.getGroup()
        if self.groupList.count > 0 {
            self.heightGroupView.constant = CGFloat(self.groupList.count * 65 + 120)
            self.mTableView.reloadData()
            
        }
    }
    
    func addgroup()  {
        viewAddGroup.isHidden = true
        viewGroup.isHidden = false
        let obj = GroupModel()
        obj.name = txtGroup.text
        self.groupList.append(obj)
        prepareGroup(obj: obj)
        self.heightGroupView.constant = CGFloat(self.groupList.count * 65 + 100)
        self.mTableView.reloadData()
    }
    
    func prepareGroup(obj: GroupModel){
        let date = Date().timeIntervalSince1970*1000
        let owner = UserDefaults.standard.object(forKey: Constant.DefaultStorage.USER_ID)
        let objGroup = GroupModel(id: "", color: "", desc: "", group_id: NSNumber(value: date), group_shared_channel: "", members: [], name: obj.name!, owner: "\(owner ?? "")", pending_members: [])
        dbHelper.saveGroup(group: objGroup)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AddGroupVC:UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groupList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellObject : NotebooksCell = tableView.dequeueReusableCell(withIdentifier: "NotebooksCell")! as! NotebooksCell
        cellObject.lblNotbook.text = "\(self.groupList[indexPath.row].name ?? "")"
        return cellObject
    }
}
extension AddGroupVC:UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate.selectedGroup(groupObj: self.groupList[indexPath.row])
        self.dismiss(animated: true, completion: nil)
    }
    
}
