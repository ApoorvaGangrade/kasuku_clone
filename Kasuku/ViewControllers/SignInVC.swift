import UIKit
import GoogleSignIn
import FBSDKLoginKit
import SwiftyJSON

class SignInVC: BaseVC {

    @IBOutlet weak var btnContinue: ButtonDesign!
    @IBOutlet weak var btnFacebook: ButtonDesign!
    @IBOutlet weak var btnGoogle: ButtonDesign!
    @IBOutlet weak var txtEmailID: LoginTextField!
    @IBOutlet weak var txtPassword: LoginTextField!
    @IBOutlet var vwPasswordReset: UIVisualEffectView!
    @IBOutlet var vwCodeReset: UIVisualEffectView!
    @IBOutlet weak var txtGetCode: LoginTextField!
    
    
    @IBAction func btnContinuePressed(_ sender: UIButton) {
        if txtEmailID.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            self.showAlert(cancelTitle: nil, buttonTitles: ["OK"], title: Constant.AlertMessages.alertTitle, message: Constant.AlertMessages.mendatoryField)
            return
        }
        
        if !(txtEmailID.text!.isValidEmail()){
            self.showAlert(cancelTitle: nil, buttonTitles: ["OK"], title: Constant.AlertMessages.alertTitle, message: Constant.AlertMessages.invalidEmail)
            return
        }
        
        if txtPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            self.showAlert(cancelTitle: nil, buttonTitles: ["OK"], title: Constant.AlertMessages.alertTitle, message: Constant.AlertMessages.mendatoryField)
            return
        }
        
        print("Now user can logged In")
        
        self.login(email: txtEmailID.text!, password: txtPassword.text!)
    }
    
    
    func login(email: String, password: String){
        self.view.endEditing(true)
        
        //Check for internet Services
        if NetworkManager.sharedInstance.isInternetAvailable(){
            
            
            //API URL & Paramaters
            let url : String = Constant.serverURL + Constant.EndPoints.login
            let parameters = ["email" : email, "password": password, "device_id": Constant.deviceID]
            let paramData = ["data": parameters]
            let sendData = ["data": paramData, "email" : email, "password": password, "device_id": Constant.deviceID] as [String : Any]
            print(url)
            print(JSON(sendData))
            
            // Adding activity indicator
            includeIndicator(sender: self)
            
            
            //Login API Call
            NetworkManager.sharedInstance.commonNetworkCall(url: url, method: .get, parameters: parameters, completionHandler: { (json, status) in
                guard let vJson = json?.dictionaryValue else {
                    self.removeIndicator(sender: self)
                    self.showAlert(cancelTitle: nil, buttonTitles: ["OK"], title: Constant.AlertMessages.alertTitle, message: Constant.AlertMessages.somethingWent)
                    return}
                 if(vJson["success"]!.boolValue){
                    print("Status: Response")
                    let data = vJson["data"]!.dictionaryValue
                    print(data)
                    let createdAt = data["createdAt"]!.intValue
                    let userId = data["id"]!.intValue
                    let userName = data["name"]!.string
                    let userProfilePic = data["profilePic"]!.string
                    let userCountry = data["country"]!.string
                    let userContactNumber = data["contactNumber"]!.string
                    let userEmail = data["email"]!.string

                    UserDefaults.standard.set(userProfilePic, forKey: Constant.DefaultStorage.USER_PROFILE)
                    UserDefaults.standard.set(userCountry, forKey: Constant.DefaultStorage.USER_COUNTRYNAME)
                    UserDefaults.standard.set(userContactNumber, forKey: Constant.DefaultStorage.USER_CONTACTNUMBER)
                    UserDefaults.standard.set(userId, forKey: Constant.DefaultStorage.USER_ID)
                    UserDefaults.standard.set(userName, forKey: Constant.DefaultStorage.USER_NAME)
                    UserDefaults.standard.set(userEmail, forKey: Constant.DefaultStorage.USER_EMAIL)
                    UserDefaults.standard.set(createdAt, forKey: Constant.DefaultStorage.USER_CREATION)
                    UserDefaults.standard.synchronize()

                   print("Created At: \(createdAt)")
                   print("------------------------")
                    print(JSON(vJson))
                    
//                    AppDelegate.sharedInstance().setRoot(name: "", email : email, createdAt: createdAt)
                    if let featured = userProfilePic{
                        self.callAppDelegateSetRootAndInitDb(name: userName!, email: email, createdAt: createdAt, profilePic: featured)
                    }
                    else {
                        self.callAppDelegateSetRootAndInitDb(name: userName!, email: email, createdAt: createdAt, profilePic: "")
                    }

                    
                }else{
                    //Remove activity indicator
                    self.removeIndicator(sender: self)
                    self.showAlert(cancelTitle: nil, buttonTitles: ["OK"], title: Constant.AlertMessages.alertTitle, message: vJson["message"]!.stringValue.replacingOccurrences(of: "_", with: " "))
                }
            })
        }else{
            //Alert for enabling Internet Services
            self.showAlert(cancelTitle: nil, buttonTitles: ["OK"], title: Constant.AlertMessages.alertTitle, message: Constant.AlertMessages.enableInternet)
        }
    }
    
    @IBAction func btnFacebookPressed(_ sender: Any) {
        print("Facebook button pressed")
        let fbLoginManager : LoginManager = LoginManager()
        fbLoginManager.logIn(permissions: ["email"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : LoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email")){
                        self.getFBUserData()
                        fbLoginManager.logOut()
                    }
                }
            }
        }
    }
    
    @IBAction func btnGooglePressed(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    
    @IBAction func btnCancelCodeResetPressed(_ sender: UIButton) {
        vwCodeReset.removeFromSuperview()
    }
    
    @IBAction func btnOkCodeResetPressed(_ sender: UIButton) {
        if txtGetCode.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            self.showAlert(cancelTitle: nil, buttonTitles: ["OK"], title: Constant.AlertMessages.alertTitle, message: Constant.AlertMessages.mendatoryField)
            return
        }
        
        if !(txtGetCode.text!.isValidEmail()){
            self.showAlert(cancelTitle: nil, buttonTitles: ["OK"], title: Constant.AlertMessages.alertTitle, message: Constant.AlertMessages.invalidEmail)
            return
        }
        
        
        let param = ["username": txtGetCode.text?.trimmingCharacters(in: .whitespacesAndNewlines), "device_id": Constant.deviceID, "type": "request"]
        let url = Constant.serverURL + Constant.EndPoints.forgotPassword
        
        print(JSON(param))
        print(url)
        
       if NetworkManager.sharedInstance.isInternetAvailable(){
        self.view.endEditing(true)
        self.includeIndicator(sender: self)
        NetworkManager.sharedInstance.commonNetworkCall(url: url, method: .post, parameters: param as [String : Any]) { (json, error) in
            guard let vJson = json else {
                self.removeIndicator(sender: self)
                return}
            self.removeIndicator(sender: self)
            print(vJson)
        }
        }else{
            self.showAlert(cancelTitle: nil, buttonTitles: ["OK"], title: Constant.AlertMessages.alertTitle, message: Constant.AlertMessages.enableInternet)
        }
        
        //vwCodeReset.removeFromSuperview()
    }
    
    @IBAction func btnSignUpPressed(_ sender: ButtonDesign) {
        self.view.endEditing(true)
        let signupVC = self.storyboard?.instantiateViewController(type: SignUpVC.self)!
        self.navigationController?.pushViewController(signupVC!, animated: true)
    }
    
    @IBAction func btnOKPressed(_ sender: UIButton) {
        self.vwPasswordReset.removeFromSuperview()
    }
    @IBAction func btnCancelPressed(_ sender: UIButton) {
        self.vwPasswordReset.removeFromSuperview()
    }
    @IBAction func btnForgotPasswordPressed(_ sender: ButtonDesign) {
        self.mutipleChoiceDialog(title: "Forgot Password", message: "Please select weather you have a password reset code or not", firstBtnTitle: "Enter Reset Code", SecondBtnTitle: "Get Reset Code", CancelBtnTitle: "Cancel") { (tap) in
            if(tap == 0){
                // Press show reset Code
                self.vwPasswordReset.frame = self.view.frame
                self.view.addSubview(self.vwPasswordReset)
            }else if(tap == 1){
                // Get Reset Code
                self.vwCodeReset.frame = self.view.frame
                self.view.addSubview(self.vwCodeReset)
            }else{
                // Just remove the options
            }
        }
    }
    
    
    func setDelegate(){
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().delegate = self
        //GIDSignIn.sharedInstance().uiDelegate = self
    }
    
    
    func getFBUserData(){
        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    let dict = result as! [String : AnyObject]
                    print(dict)
                    
                    var imageURL : String?
                    if let picture = dict["picture"]{
                        if let data = (picture as! NSDictionary).value(forKey: "data"){
                            if let url = (data as! NSDictionary).value(forKey: "url") as? String{
                                imageURL = url
                            }
                        }
                    }
                    
                    self.checkSocialLogin(email: (dict["email"] as! String), RegType: "facebook", FirstName: (dict["first_name"] as! String), LastName: (dict["last_name"] as! String), mobile: "", authToken: (dict["id"] as! String), image : imageURL)
                    
                }else{
                    self.showAlert(cancelTitle: nil, buttonTitles: ["OK"], title: Constant.AlertMessages.alertTitle, message: error!.localizedDescription)
                }
            })
        }
    }
    
    
    func checkSocialLogin(email : String, RegType : String, FirstName : String, LastName : String, mobile : String, authToken: String, image: String?){
        self.view.endEditing(true)
        
        //Check for internet Services
        if NetworkManager.sharedInstance.isInternetAvailable(){
            
            
            //API URL & Paramaters
            let url : String = Constant.serverURL + Constant.EndPoints.signUp
            let name = FirstName + " " + LastName
            let parameters = ["email" : email, "accountCreationSource": RegType, "name": name, "token":authToken, "device_id": Constant.deviceID, "password": "1234","country":""]
            let paramData = ["data": parameters]
            let sendData = ["data": paramData,"email" : email, "accountCreationSource": RegType, "name": name, "token":authToken,"device_id": Constant.deviceID,"password": "1234","country":""] as [String : Any]
            print(url)
            print(JSON(sendData))
            
            // Adding activity indicator
            includeIndicator(sender: self)
            
            
            //Login API Call
            NetworkManager.sharedInstance.commonNetworkCall(url: url, method: .post, parameters: parameters, completionHandler: { (json, status) in
                guard let vJson = json?.dictionaryValue else {
                    self.removeIndicator(sender: self)
                    self.showAlert(cancelTitle: nil, buttonTitles: ["OK"], title: Constant.AlertMessages.alertTitle, message: Constant.AlertMessages.somethingWent)
                    return}
                print(vJson)
                if(vJson["status"] != 400){
                if(vJson["success"]!.boolValue){
                     Shared.sharedInstance.imageUrl = image
                     print(JSON(vJson))
                    let data = vJson["data"]!.dictionaryValue
                    print(data)
                    let createdAt = data["createdAt"]!.intValue
                    let userId = data["id"]!.intValue
                    UserDefaults.standard.set(userId, forKey: Constant.DefaultStorage.USER_ID)
                    UserDefaults.standard.set(data["country"]!.string, forKey: Constant.DefaultStorage.USER_COUNTRYNAME)
                    UserDefaults.standard.set(data["name"]!.string, forKey: Constant.DefaultStorage.USER_NAME)
                    UserDefaults.standard.set(data["contactNumber"]!.string, forKey: Constant.DefaultStorage.USER_CONTACTNUMBER)
                    UserDefaults.standard.set(Shared.sharedInstance.imageUrl, forKey: Constant.DefaultStorage.USER_PROFILE)
                    UserDefaults.standard.set(data["email"]!.string, forKey: Constant.DefaultStorage.USER_EMAIL)
                    UserDefaults.standard.synchronize()
                    
//                  call made to method to initalize the db as well at login time
//                  AppDelegate.sharedInstance().setRoot(name: name, email : email, createdAt: createdAt)
                    self.callAppDelegateSetRootAndInitDb(name: name, email: email, createdAt: createdAt, profilePic: image!)
                }else{
                    //Remove activity indicator
                    self.removeIndicator(sender: self)
                    self.showAlert(cancelTitle: nil, buttonTitles: ["OK"], title: Constant.AlertMessages.alertTitle, message: vJson["message"]!.stringValue.replacingOccurrences(of: "_", with: " "))
                    }}else{
                    self.removeIndicator(sender: self)
                    self.showAlert(cancelTitle: nil, buttonTitles: ["OK"], title: Constant.AlertMessages.alertTitle, message: vJson["message"]!.stringValue.replacingOccurrences(of: "_", with: " "))

                }
            })
        }else{
            //Alert for enabling Internet Services
            self.showAlert(cancelTitle: nil, buttonTitles: ["OK"], title: Constant.AlertMessages.alertTitle, message: Constant.AlertMessages.enableInternet)
        }
        
    }
    
    
    
    func customizeController(){
            self.btnContinue.backgroundColor = UIColor.appThemeColor()
            self.btnContinue.configureButtonLayout()
    }
    
    func callAppDelegateSetRootAndInitDb(name:String, email:String, createdAt:Int, profilePic:String){
        AppDelegate.sharedInstance().setRoot(name: name, email: email, createdAt: createdAt, profilePic: profilePic)
        self.initalizeDatabase()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setDelegate()
        self.customizeController()
        // Do any additional setup after loading the view, typically from a nib.
        // IF USER ALREADY LOGGED IN MOVE DIRECTLY TO HOME PAGE
        if(UserDefaults.standard.bool(forKey: Constant.DefaultStorage.IS_LOGGED_IN)){
            callAppDelegateSetRootAndInitDb(
                name: UserDefaults.standard.string(forKey: Constant.DefaultStorage.USER_NAME)!,
                email: UserDefaults.standard.string(forKey: Constant.DefaultStorage.USER_EMAIL)!,
                createdAt: UserDefaults.standard.integer(forKey: Constant.DefaultStorage.USER_CREATION), profilePic:  UserDefaults.standard.string(forKey: Constant.DefaultStorage.USER_PROFILE)!)

            self.initalizeDatabase()
            
//            AppDelegate.sharedInstance().setRoot(
//                name: UserDefaults.standard.string(forKey: Constant.DefaultStorage.USER_NAME)!,
//                email : UserDefaults.standard.string(forKey: Constant.DefaultStorage.USER_EMAIL)!,
//                createdAt: UserDefaults.standard.integer(forKey: Constant.DefaultStorage.USER_CREATION))
        
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension SignInVC : GIDSignInDelegate{
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
            self.showAlert(cancelTitle: nil, buttonTitles: ["OK"], title: Constant.AlertMessages.alertTitle, message: error.localizedDescription)
        } else {
            let firstName = user.profile.givenName
            let lastName =  user.profile.familyName
            let email = user.profile.email
            if user.profile.hasImage{
               let image = user.profile.imageURL(withDimension: 100)
                self.checkSocialLogin(email: email!, RegType: "google", FirstName: firstName!, LastName: lastName!, mobile: "", authToken: user.userID!,image: image?.absoluteString)
            }else{
                 self.checkSocialLogin(email: email!, RegType: "google", FirstName: firstName!, LastName: lastName!, mobile: "", authToken: user.userID!,image: nil)
            }
           
        }
    }
//}

//extension SignInVC : GIDSignInDelegate{
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        print("inWillDispatch")
    }
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        print("dismiss")
    }
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        print("present")
    }
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        print("disconnected")
    }
}


extension SignInVC : LoginButtonDelegate{
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        if !(error != nil){
            if result?.grantedPermissions != nil {
                if((result?.grantedPermissions.contains("email"))!)
                {
                    self.getFBUserData()
                }
            }
        }else{
            self.showAlert(cancelTitle: nil, buttonTitles: ["OK"], title: Constant.AlertMessages.alertTitle, message: error!.localizedDescription)
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        print("logout clicked")
    }
    
    func loginButtonWillLogin(_ loginButton: FBLoginButton) -> Bool {
        return true
    }
}
